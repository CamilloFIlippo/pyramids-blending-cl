////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------   _PARAMS	----------------------------------------------------------------------////

enum PixelFormats{ RGB, RGBA, BGR, BGRA };

typedef struct
{
    uint width;
    uint height;
    uint channels;
    enum PixelFormats pixelFormat;
}ImageInfo;


////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _WRITE PIXEL BYTE -------------------------------------------------------------////

void write_pixel_byte(__global uchar * output, __global ImageInfo *imageInfoIn, int x,int y, uchar4 pixel )
{

    int rowOffset = 4 * imageInfoIn->width * imageInfoIn->height - (y+1) * 4 * imageInfoIn->width;
	
	int my = imageInfoIn->channels * x + rowOffset;

    if ((my>=0) && (my <  imageInfoIn->height*imageInfoIn->width*imageInfoIn->channels  ) ) {
        output[ my     ] =  pixel.x; 
        output[ my + 1 ] =  pixel.y;
        output[ my + 2 ] =  pixel.z;
        output[ my + 3 ] =  pixel.w;
    }

}

////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _WRITE PIXEL SHORT -------------------------------------------------------------////

void write_pixel_short(__global short * output, __global ImageInfo *imageInfoIn, int x,int y, short4 pixel )
{

    int rowOffset = 4 * imageInfoIn->width * imageInfoIn->height - (y+1) * 4 * imageInfoIn->width;
	
	int my = imageInfoIn->channels * x + rowOffset;

    if ((my>=0) && (my <  imageInfoIn->height*imageInfoIn->width*imageInfoIn->channels  ) ) {
        output[ my     ] =  pixel.x; 
        output[ my + 1 ] =  pixel.y;
        output[ my + 2 ] =  pixel.z;
        output[ my + 3 ] =  pixel.w;
    }

}


////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _WRITE PIXEL FLOAT4 ------------------------------------------------------------////

void write_pixel_float4(__global float4 * output, __global ImageInfo *imageInfoIn, int x,int y, float4 pixel )
{

    int rowOffset = imageInfoIn->width * imageInfoIn->height - (y+1) * imageInfoIn->width;
	
	int my = x + rowOffset;

   if ((my>=0) && (my <  imageInfoIn->height*imageInfoIn->width ) ) {
        output[my] = pixel; 
    }
 
}


////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _WRITE PIXEL FLOAT -------------------------------------------------------------////

void write_pixel_float(__global float * output, __global ImageInfo *imageInfoIn, int x,int y, float4 pixel )
{

    int rowOffset = 4 * imageInfoIn->width * imageInfoIn->height - (y+1) * 4 * imageInfoIn->width;
	
	int my = imageInfoIn->channels * x + rowOffset;

    if ((my>=0) && (my <  imageInfoIn->height*imageInfoIn->width*imageInfoIn->channels  ) ) {
        output[ my     ] =  pixel.x; 
        output[ my + 1 ] =  pixel.y;
        output[ my + 2 ] =  pixel.z;
        output[ my + 3 ] =  pixel.w;
    }

}



////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _READ PIXEL FLOAT4 -------------------------------------------------------------////

float4 read_pixel_float4(__global float4 * input, __global ImageInfo *imageInfoIn, int x,int y )
{
    if(((x<0 || y<0))||((x>imageInfoIn->width-1)||(y>imageInfoIn->height-1)))
        return (float4)(0,0,0,0);

    float4 pixel = (float4)(0,0,0,0);
    
    int rowOffset = imageInfoIn->width * imageInfoIn->height - (y+1) * imageInfoIn->width;	
	int my = x + rowOffset;


    pixel = input[my];

   
    return pixel;
}

////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _READ PIXEL BYTE -------------------------------------------------------------////

uchar4 read_pixel_byte(__global uchar * input, __global ImageInfo *imageInfoIn, int x,int y )
{
    if(((x<0 || y<0))||((x>imageInfoIn->width-1)||(y>imageInfoIn->height-1)))
        return (uchar4)(0,0,0,0);

    uchar4 pixel = (uchar4)(0,0,0,0);
    
    int rowOffset = 4 * imageInfoIn->width * imageInfoIn->height - (y+1) * 4 * imageInfoIn->width;	
	int my = imageInfoIn->channels * x + rowOffset;


    pixel.x = input[my  ];
    pixel.y = input[my+1];
    pixel.z = input[my+2];
    pixel.w = input[my+3];
   
    return pixel;
}


////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _READ PIXEL SHORT -------------------------------------------------------------////

short4 read_pixel_short(__global short * input, __global ImageInfo *imageInfoIn, int x,int y )
{
    if(((x<0 || y<0))||((x>imageInfoIn->width-1)||(y>imageInfoIn->height-1)))
        return (short4)(0,0,0,0);

    short4 pixel = (short4)(0,0,0,0);
    //int rowOffset = y * imageInfoIn->width * imageInfoIn->channels;
    int rowOffset = 4 * imageInfoIn->width * imageInfoIn->height - (y+1) * 4 * imageInfoIn->width;
	
	int my = imageInfoIn->channels * x + rowOffset;


    pixel.x = input[my  ];
    pixel.y = input[my+1];
    pixel.z = input[my+2];
    pixel.w = input[my+3];
   
    return pixel;
}


////--------------------------------------------------------------------------------------------------------------------------////
////-------------------------------------------- _READ  PIXEL CHAR -----------------------------------------------------------////

short4 read_char_pixel(__global uchar * input, __global ImageInfo *imageInfoIn, int x,int y )
{
    if(((x<0 || y<0))||((x>imageInfoIn->width-1)||(y>imageInfoIn->height-1)))
        return (short4)(0,0,0,0);

    short4 pixel = (short4)(0,0,0,0);

    int rowOffset = y * imageInfoIn->width * imageInfoIn->channels;
    int my = imageInfoIn->channels * x + rowOffset;


    pixel.x = (short)input[my  ];
    pixel.y = (short)input[my+1];
    pixel.z = (short)input[my+2];
    pixel.w = (short)input[my+3];
   
    return pixel;
}

////--------------------------------------------------------------------------------------------------------------------------////
////------------------------------------------- _READ PIXEL FLOAT ------------------------------------------------------------////

float4 read_pixel_float(__global float * input, __global ImageInfo *imageInfoIn, int x,int y )
{
    if(((x<0 || y<0))||((x>imageInfoIn->width-1)||(y>imageInfoIn->height-1)))
        return (float4)(0,0,0,0);

    float4 pixel;
    int rowOffset = 4 * imageInfoIn->width * imageInfoIn->height - (y+1) * 4 * imageInfoIn->width;
	
	int my = imageInfoIn->channels * x + rowOffset;


    pixel.x = input[my  ];
    pixel.y = input[my+1];
    pixel.z = input[my+2];
    pixel.w = input[my+3];
   
    return pixel;
}

////--------------------------------------------------------------------------------------------------------------------------////
////--------------------------------------------- _WRITE  PIXEL CHAR ---------------------------------------------------------////

void write_char_pixel(__global uchar * output, __global ImageInfo *imageInfoIn, int x,int y, short4 pixel )
{


    int rowOffset = y * imageInfoIn->width * imageInfoIn->channels;
    int my = imageInfoIn->channels * x + rowOffset;

    if ((my>=0) && (my <  imageInfoIn->height*imageInfoIn->width*imageInfoIn->channels  ) ) {
        output[ my     ] =  pixel.x; 
        output[ my + 1 ] =  pixel.y;
        output[ my + 2 ] =  pixel.z;
        output[ my + 3 ] =  pixel.w;
    }

}



void write_to_cache(__local float4 * output, int x,int y, float4 pixel , int localSize, int HALF_FILTER_SIZE, int TWICE_HALF_FILTER_SIZE)
{

	const int localRowLen =  TWICE_HALF_FILTER_SIZE + localSize;	
	const int localRowOffset = localRowLen * localRowLen - ( y + 1 + HALF_FILTER_SIZE  ) * localRowLen;
	const int myLocal = localRowOffset + x + HALF_FILTER_SIZE ;	

	output[ myLocal ] = pixel;

}

void write_to_cache2(__local float4 * output, int x,int y, float4 pixel , int localSize, int HALF_FILTER_SIZE, int TWICE_HALF_FILTER_SIZE)
{

	const int localRowLen =  TWICE_HALF_FILTER_SIZE + localSize;	
	const int localRowOffset = ( y + HALF_FILTER_SIZE  ) * localRowLen;
	const int myLocal = localRowOffset + x + HALF_FILTER_SIZE ;	

	output[ myLocal ] = pixel;

}


float4 read_cache_pixel(__local float4 * input, int x,int y, int localSize, int HALF_FILTER_SIZE, int  TWICE_HALF_FILTER_SIZE )
{    

	float4 pixel;
	const int localRowLen =  TWICE_HALF_FILTER_SIZE + localSize;
	const int localRowOffset = (localRowLen*localRowLen) - ( y + 1 + HALF_FILTER_SIZE  ) * localRowLen;
	const int myLocal = localRowOffset + x + HALF_FILTER_SIZE ;	
  
    pixel = input[myLocal];
	

    return pixel;
	
}

float4 read_cache_pixel2(__local float4 * input, int x,int y, int localSize, int HALF_FILTER_SIZE, int  TWICE_HALF_FILTER_SIZE )
{    

	float4 pixel;
	const int localRowLen =  TWICE_HALF_FILTER_SIZE + localSize;
	const int localRowOffset = ( y + HALF_FILTER_SIZE  ) * localRowLen;
	const int myLocal = localRowOffset + x + HALF_FILTER_SIZE ;	
  
    pixel = input[myLocal];
	

    return pixel;
	
}

float4 read_pixel_F4(__global float4 * input, int x,int y, int imageWidht, int imageHeight )
{

    if(((x<0 || y<0))||((x>imageWidht-1)||(y>imageHeight-1)))
        return (float4)(0,0,0,0);
    
    int rowOffset = (imageWidht*imageHeight) - (y+1) * imageWidht;	
	int index = x + rowOffset;
   
    return input[index];
}

float4 read_pixel_F42(__global float4 * input, int x,int y, int imageWidht, int imageHeight )
{

    if(((x<0 || y<0))||((x>imageWidht-1)||(y>imageHeight-1)))
        return (float4)(0,0,0,0);
    
    int rowOffset =  y * imageWidht;	
	int index = x + rowOffset;
   
    return input[index];
}

float4 read_pixel_offset(__global float4 * input, int x,int y, int offset, int imageWidth, int imageHeight  )
{

    if(((x<0 || y<0))||((x>imageWidth-1)||(y>imageHeight-1)))
        return (float4)(0,0,0,0);

    float4 pixel = (float4)(0,0,0,0);
    int rowOffset = imageWidth * imageHeight - (y+1) * imageWidth;
	int my = x + rowOffset;

    pixel = input[my+ offset];

    return pixel;
}

void write_pixel_F4(__global float4 * output, int x,int y, float4 pixel, int imageWidth, int imageHeight )
{

	int rowOffset = imageWidth * imageHeight - (y+1) * imageWidth;

	int my = x + rowOffset;

   if ((my>=0) && (my <  imageHeight * imageWidth) ) {
        output[my] = pixel; 
    }
 
}



 __kernel void subtract_images_4F(const __global float4 *input,
                                  const __global float4 *input2, 
									    __global float4* output,
		  								const int imageWidth,
										const int imageHeight)                          
 {
  
	int gID_X = get_global_id(0);
	int gID_Y = get_global_id(1);

	float4 pixelIm1 = read_pixel_F4(input, gID_X, gID_Y, imageWidth, imageHeight);
	float4 pixelIm2 = read_pixel_F4(input2, gID_X, gID_Y, imageWidth, imageHeight);
	float4 subtractResult;

	if(pixelIm1.w>0 && pixelIm2.w >0)
	{
			subtractResult = pixelIm1 - pixelIm2;
			subtractResult.w = pixelIm2.w;
	}
	else if(pixelIm2.w>0)
	{
		 subtractResult = (float4)(0,0,0,0);
	}
	else
	{
		 subtractResult = (float4)(0,0,0,0);
	}
	
	write_pixel_F4(output, gID_X, gID_Y, subtractResult, imageWidth, imageHeight);
	
		 
 }

 ////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _PYRAMID DOWN FLOAT4 -----------------------------------------------------------////

 __kernel void pyramid_down_4F(
    const __global  float4 *  input,
          __global  float4 *  output,
          __constant float4 * filter,
		  __local float4 * cached,
		  	const int imageWidth,
			const int imageHeight)
{
	
	int2 coord = (int2) ( get_global_id(0), get_global_id(1));
	int2 coordLoc = (int2) ( get_local_id(0), get_local_id(1));
 

	float4 sum = 0;
	int fIndex=0;
	int FILTER_SIZE = 5;
	int HALF_FILTER_SIZE = FILTER_SIZE/2;
	int TWICE_HALF_FILTER_SIZE = HALF_FILTER_SIZE *2; 
	float4 calculateFactor=0;

	float4 initPixel = read_pixel_F4(input, coord.x*2, coord.y*2, imageWidth*2, imageHeight*2);
	
	float4 cachePixel = read_pixel_F4(input, coord.x*2, coord.y*2, imageWidth*2, imageHeight*2);
	write_to_cache(cached, coordLoc.x, coordLoc.y, cachePixel, get_local_size(0), HALF_FILTER_SIZE, TWICE_HALF_FILTER_SIZE);

	// sync
	barrier(CLK_LOCAL_MEM_FENCE);


	if(initPixel.w>0)
	{
	
		for (int r = -HALF_FILTER_SIZE; r <= HALF_FILTER_SIZE; r++)
		{
			  for (int c = -HALF_FILTER_SIZE; c<= HALF_FILTER_SIZE; c++, fIndex++)
			 {
					int x = coord.x*2 - r ;
					int y = coord.y*2 - c;
			    
					float4 pixel = read_pixel_F4(input, x, y, imageWidth*2, imageHeight*2);
		 
					if (pixel.w>0)  {
						sum += pixel * filter[fIndex];  
						calculateFactor+=filter[fIndex];       
					}
					

					/*
					float4 pixel = read_cache_pixel(cached, coordLoc.x - r, coordLoc.y - c, get_local_size(0), HALF_FILTER_SIZE, TWICE_HALF_FILTER_SIZE );
					if(pixel.w>0)
					{
						sum += pixel * filter[ fIndex ]; 
						calculateFactor+=filter[fIndex]; 
					} 
					*/					 	
			 }
			 
		}

		sum = sum / calculateFactor;

		write_pixel_F4(output, coord.x, coord.y, sum, imageWidth, imageHeight);
	}
	else
	{
		write_pixel_F4(output, coord.x, coord.y, (float4)(0,0,0,0), imageWidth, imageHeight);
	}
	
}

////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _PYRAMID UP FLOAT4 -----------------------------------------------------------////

 __kernel void pyramid_up_4F(
	const __global  float4 *  input,
          __global  float4 *  output,
          __constant float4 * filter,
		  __local float4 * cached,
			const int imageWidth,
			const int imageHeight)
{
		float x,y, x2, y2;
		float4 sum=0;
		int fIndex=0;
		int FILTER_SIZE = 5;
		int HALF_FILTER_SIZE = FILTER_SIZE/2;
		int TWICE_HALF_FILTER_SIZE = HALF_FILTER_SIZE*2;
		int2 coord = (int2) ( get_global_id(0), get_global_id(1));
		int2 coordLoc = (int2) ( get_local_id(0), get_local_id(1));
		float4 calculateFactor=0;

		float4 pixelCache = read_pixel_F4(input, coord.x/2, coord.y/2, imageWidth/2, imageHeight/2);
		write_to_cache(cached, coordLoc.x, coordLoc.y, pixelCache, get_local_size(0), HALF_FILTER_SIZE, TWICE_HALF_FILTER_SIZE);

		// sync
		barrier(CLK_LOCAL_MEM_FENCE);

		for (int r = -HALF_FILTER_SIZE; r <= HALF_FILTER_SIZE; r++)
		{
				 x = (coord.x - r)/2;
				 x2 = (coordLoc.x - r)/2;

				 for (int c = -HALF_FILTER_SIZE; c<= HALF_FILTER_SIZE; c++, fIndex++)
				 {
					
						y = (coord.y - c)/2;
						y2 = (coordLoc.y - c)/2;

					    if(x == (int)x && y == (int)y)
						{
							 float4 pixel = read_pixel_F4(input, x, y, imageWidth/2, imageHeight/2);
							 if(pixel.w >0)
							 {
							 	sum += pixel * filter[fIndex];
								calculateFactor += filter[fIndex];    
							 }
						}			
						
						
					/*	if(x2 == (int)x2 && y2 == (int)y2)
						{
							 float4 pixel = read_cache_pixel(cached, coordLoc.x - r, coordLoc.y - c, get_local_size(0), HALF_FILTER_SIZE, TWICE_HALF_FILTER_SIZE );
							 if(pixel.w >0)
							 {
							 	sum += pixel * filter[fIndex];   
							 }
						}	
						*/
				 }
			 
		}
		sum = sum/calculateFactor;

		write_pixel_F4(output, coord.x, coord.y, sum, imageWidth, imageHeight);
	
}


////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _PYRAMID DOWN ------------------------------------------------------------------////

 __kernel void pyramid_down(
    const __global  short * const input,
          __global  short * const output,
          __constant float * const mask,
          __global ImageInfo *imageInfoIn,
          __global ImageInfo *imageInfoOut,
          const int maskWidth, 
		  const int width, 
		  const int height)
{

	imageInfoOut->width = width;
	imageInfoOut->height = height;
	imageInfoOut->channels = 4;																	//----------------- CHANNELS AS CONST
	

    int2 coord = (int2) ( get_global_id(0), get_global_id(1));
    float sumR = 0;
    float sumG = 0;
    float sumB = 0;
	float sumA = 0;
    float calculateFactor=0;
    int offset = maskWidth/2;


    for (int r = -maskWidth/2; r <= maskWidth/2; r++)
    {
         for (int c = -maskWidth/2; c<= maskWidth/2; c++)
         {
        
             int x = coord.x*2 - r ;
             int y = coord.y*2 - c;
                
             short4 pixel = read_pixel_short(input, imageInfoIn, x, y);
                        
             if (pixel.w>0)  {
                  sumR += (float)(pixel.x * mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
                  sumG += (float)(pixel.y * mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
                  sumB += (float)(pixel.z * mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
				  sumA += (float)(pixel.w * mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
                  calculateFactor +=(mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
              }                            
          }
    }

    short4 pixelOut;
	pixelOut.x = (short)((float)sumR/calculateFactor);
	pixelOut.y = (short)((float)sumG/calculateFactor);
	pixelOut.z = (short)((float)sumB/calculateFactor);
	pixelOut.w = (short)((float)sumA/calculateFactor);

    short4 pixel = read_pixel_short(input, imageInfoIn, coord.x*2, coord.y*2);


	if(pixel.w>0)
	{
		write_pixel_short(output,imageInfoOut, coord.x, coord.y, pixelOut);
	}
	else
	{
		write_pixel_short(output,imageInfoOut, coord.x, coord.y, (short4)(0,0,0,0));
	}
	

}



 __kernel void pyramid_down_test(
    const __global  short * const input,
          __global  short * const output,
          __constant short * const mask,
          __global ImageInfo *imageInfoIn,
          __global ImageInfo *imageInfoOut,
          const int maskWidth, 
		  const int width, 
		  const int height)
{

	imageInfoOut->width = width;
	imageInfoOut->height = height;
	imageInfoOut->channels = 4;																	//----------------- CHANNELS AS CONST

	

	int2 coord = (int2) ( get_global_id(0), get_global_id(1));
	short4 pixel = read_pixel_short(input, imageInfoIn, coord.x*2, coord.y*2);

	//if(pixel.w>0)
	//{
		
		float sumR = 0;
		float sumG = 0;
		float sumB = 0;
		float sumA = 0;
		float calculateFactor=0;
		int offset = maskWidth/2;
    
		float x,y;
        
		for (int r = -maskWidth/2; r <= maskWidth/2; r++)
		{
			 for (int c = -maskWidth/2; c<= maskWidth/2; c++)
			 {
        
				 int x = coord.x*2 - r ;
				 int y = coord.y*2 - c;
                
				 short4 pixel = read_pixel_short(input, imageInfoIn, x, y);
                        
				 if (pixel.w>0)  {
					  sumR += (float)(pixel.x * mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
					  sumG += (float)(pixel.y * mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
					  sumB += (float)(pixel.z * mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
					  sumA += (float)(pixel.w * mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
					  calculateFactor +=(mask[(r+(maskWidth/2))*maskWidth + (c+(maskWidth/2))]);
				  }                            
			  }
		}

		short4 pixelOut;
		pixelOut.x = (short)((float)sumR/calculateFactor);
		pixelOut.y = (short)((float)sumG/calculateFactor);
		pixelOut.z = (short)((float)sumB/calculateFactor);
		pixelOut.w = (short)((float)sumA/calculateFactor);
		write_pixel_short(output,imageInfoOut, coord.x, coord.y, pixelOut);

//	}
//	else
//	{
//		write_pixel_short(output,imageInfoOut, coord.x, coord.y, (short4)(0,0,0,0));
//	}

}



////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- _PYRAMID UP --------------------------------------------------------------------////

 __kernel void pyramid_up(
    const __global  short * const input,
          __global  short * const output,
          __constant float * const mask,
          __global ImageInfo *imageInfoIn,
          __global ImageInfo *imageInfoOut,
          const int maskWidth,
		  const int outWidth,
		  const int outHeight)
{

    int2 input_coord = (int2)(get_global_id(0) ,get_global_id(1));
   
	imageInfoOut->width = outWidth;
	imageInfoOut->height = outHeight;
	imageInfoOut->channels = 4;										//----------------------------> Same channels hardcoded


    short4 pixelR;

    float sumR=0;
    float sumG=0;
    float sumB=0;
    float sumA=0;
    float calculateFactor=0;
    float x;
    float y;

     
    for(int m =-maskWidth/2; m<=maskWidth/2; m++)
    {
        x= (input_coord.x - m)/2;
            for(int n=-maskWidth/2; n<=maskWidth/2; n++)
            {

                
                y= (input_coord.y - n)/2;

                if(x == (int)x && y == (int)y)
                {
                    pixelR = read_pixel_short(input, imageInfoIn, x, y);
                    if(pixelR.w >0)
                    {
                        sumR += (float)(pixelR.x * mask[(m+maskWidth/2)*maskWidth+n+maskWidth/2]);
                        sumG += (float)(pixelR.y * mask[(m+maskWidth/2)*maskWidth+n+maskWidth/2]);
                        sumB += (float)(pixelR.z * mask[(m+maskWidth/2)*maskWidth+n+maskWidth/2]);
						sumA += (float)(pixelR.w * mask[(m+maskWidth/2)*maskWidth+n+maskWidth/2]);
                        calculateFactor += (float)(mask[(m+maskWidth/2)*maskWidth+n+maskWidth/2]);                                                    
                        
                    }
                }
            }
    }

    short4 pixelOut;
    pixelOut.x = (short)(sumR/calculateFactor);
    pixelOut.y = (short)(sumG/calculateFactor);
    pixelOut.z = (short)(sumB/calculateFactor);
    pixelOut.w = (short)(sumA/calculateFactor);

    write_pixel_short(output, imageInfoOut, input_coord.x, input_coord.y, pixelOut);
	
}

////--------------------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------  SHORT TO BYTE  ---------------------------------------------------------------------////


__kernel void short_to_byte(__global short *input , __global short* output, __global ImageInfo *imageInfoIn, __global ImageInfo *imageInfoOut)
{
    imageInfoOut->width = imageInfoIn->width;
    imageInfoOut->height = imageInfoIn->height;
    imageInfoOut->channels = imageInfoIn->channels;

    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    
    
    short4 pixel =  read_pixel_short(input, imageInfoIn, coord.x, coord.y);
    write_char_pixel(output, imageInfoOut, coord.x,coord.y, pixel );
}

////--------------------------------------------------------------------------------------------------------------------------------------////
////---------------------------------------- BYTE TO SHORT    ---------------------------------------------------------------------////

__kernel void byte_to_short(__global uchar *input , __global short* output, __global ImageInfo *imageInfoIn, __global ImageInfo *imageInfoOut)
{
    imageInfoOut->width = imageInfoIn->width;
    imageInfoOut->height = imageInfoIn->height;
    imageInfoOut->channels = imageInfoIn->channels;

    int x = get_global_id(0);
    int y = get_global_id(1);

    
    short4 pixel =  read_char_pixel(input, imageInfoOut, x, y);
    write_pixel_short(output, imageInfoOut, x, y,  pixel);
}

////--------------------------------------------------------------------------------------------------------------------------------------////
////---------------------------------------- BYTE TO FLOAT    ---------------------------------------------------------------------////

__kernel void byte_to_float(__global uchar *input , __global float* output, __global ImageInfo *imageInfoIn, __global ImageInfo *imageInfoOut)
{
    imageInfoOut->width = imageInfoIn->width;
    imageInfoOut->height = imageInfoIn->height;
    imageInfoOut->channels = imageInfoIn->channels;

	int x = get_global_id(0);
    int y = get_global_id(1);

	uchar4 pixelByte =  read_pixel_byte(input, imageInfoOut, x, y);
	float4 pixelFloat;
	pixelFloat.x = (float)(pixelByte.x/(float)255);
	pixelFloat.y = (float)(pixelByte.y/(float)255);
	pixelFloat.z = (float)(pixelByte.z/(float)255);
	pixelFloat.w = (float)(pixelByte.w/(float)255);

	write_pixel_float(output, imageInfoOut,x,y, pixelFloat);

}


////--------------------------------------------------------------------------------------------------------------------------------------////
////---------------------------------------- BYTE TO FLOAT4    ---------------------------------------------------------------------////

__kernel void byte_to_float4(__global uchar *input , __global float4* output, __global ImageInfo *imageInfoIn, __global ImageInfo *imageInfoOut)
{
    imageInfoOut->width = imageInfoIn->width;
    imageInfoOut->height = imageInfoIn->height;
    imageInfoOut->channels = imageInfoIn->channels;

	int x = get_global_id(0);
    int y = get_global_id(1);

	uchar4 pixelByte =  read_pixel_byte(input, imageInfoOut, x, y);
	float4 pixelFloat;

	/*
	pixelFloat.x = (float)(pixelByte.x/(float)255);
	pixelFloat.y = (float)(pixelByte.y/(float)255);
	pixelFloat.z = (float)(pixelByte.z/(float)255);
	pixelFloat.w = (float)(pixelByte.w/(float)255);
	*/
	pixelFloat = convert_float4(pixelByte)/255.0;

	write_pixel_float4(output, imageInfoOut, x, y, pixelFloat);

}


////--------------------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------  FLOAT4 TO BYTE    ---------------------------------------------------------------------////

__kernel void float4_to_byte(__global float4 *input , __global uchar* output, __global ImageInfo *imageInfoIn, __global ImageInfo *imageInfoOut)
{
	
    imageInfoOut->width = imageInfoIn->width;
    imageInfoOut->height = imageInfoIn->height;
    imageInfoOut->channels = imageInfoIn->channels; 

	int x = get_global_id(0);
	int y = get_global_id(1);

	float4 pixelFloat4 = read_pixel_float4(input, imageInfoIn, x, y);
	uchar4 pixelByte;

	pixelByte.x = pixelFloat4.x * 255;
	pixelByte.y = pixelFloat4.y * 255;
	pixelByte.z = pixelFloat4.z * 255;
	pixelByte.w = pixelFloat4.w * 255;

	write_pixel_byte(output, imageInfoOut, x, y, pixelByte);
	
}


////--------------------------------------------------------------------------------------------------------------------------------------////
////---------------------------------------- FLOAT TO BYTE    ---------------------------------------------------------------------////

__kernel void float_to_byte(__global float *input , __global uchar* output, __global ImageInfo *imageInfoIn, __global ImageInfo *imageInfoOut)
{
	imageInfoOut->width = imageInfoIn->width;
    imageInfoOut->height = imageInfoIn->height;
    imageInfoOut->channels = imageInfoIn->channels;

	int x = get_global_id(0);
    int y = get_global_id(1);

	float4 pixelFloat = read_pixel_float(input, imageInfoIn, x, y);
	uchar4 pixelByte;
	
	pixelByte.x = pixelFloat.x * 255;
	pixelByte.y = pixelFloat.y * 255;
	pixelByte.z = pixelFloat.z * 255;
	pixelByte.w = pixelFloat.w * 255;

	write_pixel_byte(output, imageInfoOut, x, y, pixelByte);

}


////--------------------------------------------------------------------------------------------------------------------------------------////
////--------------------------------------------  SUBTRACT IMAGES    ---------------------------------------------------------------------////

 __kernel void subtract_images(__global short *input,
                               __global short *input2, 
                               __global short* output,
                               __global ImageInfo *imageInfoIn,
                               __global ImageInfo *imageInfoIn2,
                               __global ImageInfo *imageInfoOut)
 {
    imageInfoOut->width =   imageInfoIn->width;
    imageInfoOut->height =  imageInfoIn->height;
    imageInfoOut->channels = imageInfoIn->channels; 
    
    
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    
    short4 pixelIm1 = read_pixel_short(input, imageInfoIn, coord.x, coord.y);
    short4 pixelIm2 = read_pixel_short(input2, imageInfoIn2, coord.x, coord.y);
    
    short4 subtractResult;

 
	if(pixelIm1.w>0 && pixelIm2.w >0)
	{
			subtractResult = pixelIm1 - pixelIm2;
			subtractResult.w = pixelIm2.z;
	}
	else if(pixelIm2.w>0)
	{
		 subtractResult = (short4)(0,0,0,0);
	}
	else
	{
		 subtractResult = (short4)(0,0,0,0);
	}

	 write_pixel_short(output, imageInfoOut, coord.x, coord.y, subtractResult);
		 
 }


////--------------------------------------------------------------------------------------------------------------------------------------////
////--------------------------------------------------- EXTEND MASK ----------------------------------------------------------------------////


  __kernel void extend_mask(const __global float4 *input,
						    const __global float4 *input2,
                                  __global float4* output,
									  const int imageWidth,
									  const int imageHeight)
{
		int gID_X = get_global_id(0);		
		int gID_Y = get_global_id(1);		 

		float4 leftPixel = read_pixel_F4(input, gID_X, gID_Y, imageWidth, imageHeight);
		float4 rightPixel = read_pixel_F4(input2, gID_X, gID_Y, imageWidth, imageHeight);
		
		
	    if(leftPixel.w>0 && rightPixel.w>0)																	// only for 3 overriding pixels
		{
			float4 pixel;			
			pixel.x = leftPixel.x + 1;
			pixel.y = leftPixel.y + 1;
			pixel.z = leftPixel.z + 1;
			pixel.w = 1;
			write_pixel_F4(output, gID_X, gID_Y, pixel, imageWidth, imageHeight);		
		}
		else if(leftPixel.w>0 || rightPixel.w>0)
		{
			if(leftPixel.x ==0)
			{
				write_pixel_F4(output, gID_X, gID_Y, (float4)(1,1,1,1), imageWidth, imageHeight);	
			}
			else
			{
				write_pixel_F4(output, gID_X, gID_Y, leftPixel, imageWidth, imageHeight);	
			}
		}
		else 
		{
			write_pixel_F4(output, gID_X, gID_Y, (float4)(0,0,0,0), imageWidth, imageHeight);	
		}
}



 ////--------------------------------------------------------------------------------------------------------------------------------------////
////---------------------------------------------------- TEST KERNEL  ---------------------------------------------------------------------////

 __kernel void test_kernel(__global float4* input, 
						   __global float4* output, 
						   __global float4 *filter,
						   __local	float4 *cached,
						   const int imageWidth, 
						   const int imageHeight)
 {

	int HALF_FILTER_SIZE = 2;
	int TWICE_HALF_FILTER_SIZE = 4;
	int IMAGE_W = imageWidth;
	int IMAGE_H = imageHeight;
	int HALF_FILTER_SIZE_IMAGE_W = HALF_FILTER_SIZE * IMAGE_W ;

	const int rowOffset = get_global_id(1) * IMAGE_W;
	const int my = get_global_id(0) + rowOffset;
	
	const int localRowLen = TWICE_HALF_FILTER_SIZE + get_local_size(0);
	const int localRowOffset = ( get_local_id(1) + HALF_FILTER_SIZE ) * localRowLen;
	const int myLocal = localRowOffset + get_local_id(0) + HALF_FILTER_SIZE;		
		
	// copy my pixel

	float4 cachePixel = read_pixel_F42(input, get_global_id(0), get_global_id(1), imageWidth, imageHeight);
	write_to_cache2(cached, get_local_id(0), get_local_id(1), cachePixel, get_local_size(0), HALF_FILTER_SIZE, TWICE_HALF_FILTER_SIZE);


	//cached[ myLocal ] = cachePixel;

	
	if (
		get_global_id(0) < HALF_FILTER_SIZE 			|| 
		get_global_id(0) > IMAGE_W - HALF_FILTER_SIZE - 1		|| 
		get_global_id(1) < HALF_FILTER_SIZE			||
		get_global_id(1) > IMAGE_H - HALF_FILTER_SIZE - 1
	)
	{
		// no computation for me, sync and exit
		//barrier(CLK_LOCAL_MEM_FENCE);
		return;
	}
	else 
	{
		// copy additional elements
		int localColOffset = -1;
		int globalColOffset = -1;
		
		if ( get_local_id(0) < HALF_FILTER_SIZE )
		{
			localColOffset = get_local_id(0);
			globalColOffset = -HALF_FILTER_SIZE;
			
			cached[ localRowOffset + get_local_id(0) ] = input[ my - HALF_FILTER_SIZE ];
		}
		else if ( get_local_id(0) >= get_local_size(0) - HALF_FILTER_SIZE )
		{
			localColOffset = get_local_id(0) + TWICE_HALF_FILTER_SIZE;
			globalColOffset = HALF_FILTER_SIZE;
			
			cached[ myLocal + HALF_FILTER_SIZE ] = input[ my + HALF_FILTER_SIZE ];
		}
		
		
		if ( get_local_id(1) < HALF_FILTER_SIZE )
		{
			cached[ get_local_id(1) * localRowLen + get_local_id(0) + HALF_FILTER_SIZE ] = input[ my - HALF_FILTER_SIZE_IMAGE_W ];
			if (localColOffset > 0)
			{
				cached[ get_local_id(1) * localRowLen + localColOffset ] = input[ my - HALF_FILTER_SIZE_IMAGE_W + globalColOffset ];
			}
		}
		else if ( get_local_id(1) >= get_local_size(1) -HALF_FILTER_SIZE )
		{
			int offset = ( get_local_id(1) + TWICE_HALF_FILTER_SIZE ) * localRowLen;
			cached[ offset + get_local_id(0) + HALF_FILTER_SIZE ] = input[ my + HALF_FILTER_SIZE_IMAGE_W ];
			if (localColOffset > 0)
			{
				cached[ offset + localColOffset ] = input[ my + HALF_FILTER_SIZE_IMAGE_W + globalColOffset ];
			}
		}
		
		// sync
		barrier(CLK_LOCAL_MEM_FENCE);

		
		// perform convolution
		int fIndex = 0;
		float4 sum = (float4) 0.0;
		
		for (int r = -HALF_FILTER_SIZE; r <= HALF_FILTER_SIZE; r++)
		{
			int curRow = r * localRowLen;
			for (int c = -HALF_FILTER_SIZE; c <= HALF_FILTER_SIZE; c++, fIndex++)
			{	
			//	sum += cached[ myLocal + curRow + c ] * filter[ fIndex ]; 
			
				
				float4 pixel = read_cache_pixel2(cached, get_local_id(0) - r, get_local_id(1) - c, get_local_size(0), HALF_FILTER_SIZE, TWICE_HALF_FILTER_SIZE );
				if(pixel.w>0)
				{
					sum += pixel * filter[ fIndex ]; 
				//	calculateFactor+=filter[fIndex]; 
				} 
					
			
			
			}
		}

		write_pixel_F4(output, get_global_id(0)/2, get_global_id(1)/2, sum, imageWidth/2, imageHeight/2);
		//write_pixel_F4(output, gID_X, gID_Y, outPixel, imageWidth, imageHeight);


		//output[my] = sum;
	}

 }

  __kernel void test_kernel2(__global float4* input, 
						   __global float4* output, 
						   __global float4 *filter,
						   __local	float4 *cached,
						   const int imageWidth, 
						   const int imageHeight)
 {

		int HALF_FILTER_SIZE = 2;
	int TWICE_HALF_FILTER_SIZE = 4;
	int IMAGE_W = imageWidth;
	int IMAGE_H = imageHeight;
	int HALF_FILTER_SIZE_IMAGE_W = HALF_FILTER_SIZE * IMAGE_W/2 ;

	const int rowOffset = get_global_id(1)/2 * IMAGE_W/2;
	const int my = get_global_id(0)/2 + rowOffset;
	
	const int localRowLen = TWICE_HALF_FILTER_SIZE + get_local_size(0);
	const int localRowOffset = ( get_local_id(1) + HALF_FILTER_SIZE ) * localRowLen;
	const int myLocal = localRowOffset + get_local_id(0) + HALF_FILTER_SIZE;		
		
	// copy my pixel

	float4 cachePixel = read_pixel_F42(input, get_global_id(0)/2, get_global_id(1)/2, imageWidth/2, imageHeight/2);
	write_to_cache2(cached, get_local_id(0), get_local_id(1), cachePixel, get_local_size(0), HALF_FILTER_SIZE, TWICE_HALF_FILTER_SIZE);


	//cached[ myLocal ] = cachePixel;

	
	if (
		get_global_id(0) < HALF_FILTER_SIZE 			|| 
		get_global_id(0) > IMAGE_W - HALF_FILTER_SIZE - 1		|| 
		get_global_id(1) < HALF_FILTER_SIZE			||
		get_global_id(1) > IMAGE_H - HALF_FILTER_SIZE - 1
	)
	{
		// no computation for me, sync and exit
		//barrier(CLK_LOCAL_MEM_FENCE);
		return;
	}
	else 
	{
		// copy additional elements
		int localColOffset = -1;
		int globalColOffset = -1;
		
		if ( get_local_id(0) < HALF_FILTER_SIZE )
		{
			localColOffset = get_local_id(0);
			globalColOffset = -HALF_FILTER_SIZE;
			
			cached[ localRowOffset + get_local_id(0) ] = input[ my - HALF_FILTER_SIZE ];
		}
		else if ( get_local_id(0) >= get_local_size(0) - HALF_FILTER_SIZE )
		{
			localColOffset = get_local_id(0) + TWICE_HALF_FILTER_SIZE;
			globalColOffset = HALF_FILTER_SIZE;
			
			cached[ myLocal + HALF_FILTER_SIZE ] = input[ my + HALF_FILTER_SIZE ];
		}
		
		
		if ( get_local_id(1) < HALF_FILTER_SIZE )
		{
			cached[ get_local_id(1) * localRowLen + get_local_id(0) + HALF_FILTER_SIZE ] = input[ my - HALF_FILTER_SIZE_IMAGE_W ];
			if (localColOffset > 0)
			{
				cached[ get_local_id(1) * localRowLen + localColOffset ] = input[ my - HALF_FILTER_SIZE_IMAGE_W + globalColOffset ];
			}
		}
		else if ( get_local_id(1) >= get_local_size(1) -HALF_FILTER_SIZE )
		{
			int offset = ( get_local_id(1) + TWICE_HALF_FILTER_SIZE ) * localRowLen;
			cached[ offset + get_local_id(0) + HALF_FILTER_SIZE ] = input[ my + HALF_FILTER_SIZE_IMAGE_W ];
			if (localColOffset > 0)
			{
				cached[ offset + localColOffset ] = input[ my + HALF_FILTER_SIZE_IMAGE_W + globalColOffset ];
			}
		}
		
		// sync
		barrier(CLK_LOCAL_MEM_FENCE);

		
		// perform convolution
		int fIndex = 0;
		float4 sum = (float4) 0.0;
		
		for (int r = -HALF_FILTER_SIZE; r <= HALF_FILTER_SIZE; r++)
		{
			int curRow = r * localRowLen;
			for (int c = -HALF_FILTER_SIZE; c <= HALF_FILTER_SIZE; c++, fIndex++)
			{	
			//	sum += cached[ myLocal + curRow + c ] * filter[ fIndex ]; 
			
				
				float4 pixel = read_cache_pixel2(cached, get_local_id(0) - r, get_local_id(1) - c, get_local_size(0), HALF_FILTER_SIZE, TWICE_HALF_FILTER_SIZE );
				if(pixel.w>0)
				{
					sum += pixel * filter[ fIndex ]; 
				//	calculateFactor+=filter[fIndex]; 
				} 
					
			
			
			}
		}

		write_pixel_F4(output, get_global_id(0), get_global_id(1), sum, imageWidth, imageHeight);
		//write_pixel_F4(output, gID_X, gID_Y, outPixel, imageWidth, imageHeight);


		//output[my] = sum;
	}

 }


////--------------------------------------------------------------------------------------------------------------------------------------////
////---------------------------------------------------  BUILD MASK ----------------------------------------------------------------------////



   __kernel void build_mask(const __global float4* input,
						    const __global float4* input2,
                                  __global float4* output,
                                   const int imageWidth,
								   const int imageHeight)
{
	
	int gID_X = get_global_id(0);
	int gID_Y = get_global_id(1);

	float4 leftPixel = read_pixel_F4(input, gID_X, gID_Y, imageWidth, imageHeight);
	float4 rightPixel = read_pixel_F4(input2, gID_X, gID_Y, imageWidth, imageHeight);
	float4 outPixel;

	if(leftPixel.w>0 && rightPixel.w>0)
	{
		outPixel = (float4)(2,2,2,1);
	}
	else if(leftPixel.w>0 || rightPixel.w>0)
	{
		outPixel = (float4)(1,1,1,1);
	}
	else 
	{
		outPixel = (float4)(0,0,0,0);
	}

	write_pixel_F4(output, gID_X, gID_Y, outPixel, imageWidth, imageHeight);

}

////----------------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------------------RECONSTRUCT MASK ---------------------------------------------------------------------////


 __kernel void reconstruct_mask(const __global float4 *input,
								      __global float4* output,
								         const int imageWidth,
								         const int imageHeight)
{
	
		int gID_X = get_global_id(0);
		int gID_Y = get_global_id(1);


		float4 outPixel = read_pixel_F4(input, gID_X, gID_Y, imageWidth, imageHeight);

		if(outPixel.w>0)
		{
			outPixel.x = 1 / outPixel.x;
			outPixel.y = 1 / outPixel.y;
			outPixel.z = 1 / outPixel.z;
			outPixel.w = 1;		
		}
		else
		{
			outPixel = (float4)(0,0,0,0);
		}

	    write_pixel_F4(output, gID_X, gID_Y, outPixel, imageWidth, imageHeight);		
}

////--------------------------------------------------------------------------------------------------------------------------------------////
////---------------------------------------------------- RESIZE DOWN ---------------------------------------------------------------------////

 __kernel void resize_down(__global float4* input,
						   __global float4* output, 
						   __local  float4* cache,
						   const int imageWidth,
						   const int imageHeight)
 {
	int2 coord = (int2)(get_global_id(0), get_global_id(1));
	int2 coordTwice = (int2)(get_global_id(0)<<1, get_global_id(1)<<1);

	int twiceWidth = imageWidth<<1;
	int twiceHeight = imageHeight<<1;

	float4 pixelOut = read_pixel_F4(input, coord.x*2, coord.y*2, imageWidth*2,  imageHeight*2);
	
	write_pixel_F4(output, coord.x, coord.y, pixelOut, imageWidth, imageHeight);

 }




 __kernel void matrix_multiplication(const __global float4* input,
									 const __global float4* input2, 
									       __global float4* output,
										     const  int     imageWidth,
											 const  int     imageHeight)  									 									
 {
	int gID_X = get_global_id(0);
	int gID_Y = get_global_id(1);

	float4 leftPixel =  read_pixel_F4(input, gID_X, gID_Y, imageWidth, imageHeight);
	float4 rightPixel = read_pixel_F4(input2, gID_X, gID_Y, imageWidth, imageHeight);

	float4 multiplyResult = leftPixel * rightPixel;

	if(leftPixel.w > 0)
	{
		multiplyResult.w = 1 ;
	}
	else
	{
		multiplyResult = (float4)(0,0,0,0);
	}


	 write_pixel_F4(output, gID_X, gID_Y, multiplyResult, imageWidth, imageHeight);
 }



 __kernel void add_images(const __global float4 *input,
                          const __global float4 *input2, 
                                __global float4 *output,
									const int imageWidth,
									const int imageHeight)                 
 {

    int gID_X = get_global_id(0);
	int gID_Y = get_global_id(1);

	float4 pixelIm1 = read_pixel_F4(input, gID_X, gID_Y, imageWidth, imageHeight);
	float4 pixelIm2 = read_pixel_F4(input2, gID_X, gID_Y, imageWidth, imageHeight);


	float4 addResult = pixelIm1 + pixelIm2;

	
	write_pixel_F4(output, gID_X, gID_Y, addResult, imageWidth, imageHeight);
 }


 __kernel void add_images_blending(const __global float4 *input,
							  	   const __global float4 *input2, 
										 __global float4 *output,
											const int imageWidth,
											const int imageHeight)                 
 {

    int gID_X = get_global_id(0);
	int gID_Y = get_global_id(1);

	float4 pixelIm1 = read_pixel_F4(input, gID_X, gID_Y, imageWidth, imageHeight);
	float4 pixelIm2 = read_pixel_F4(input2, gID_X, gID_Y, imageWidth, imageHeight);
	float4 addResult;

	if((pixelIm1.x + pixelIm2.x)>=1) 
	{
		addResult.x = 1;
	}
	else if((pixelIm1.x + pixelIm2.x)>0 )
	{
		addResult.x = pixelIm1.x + pixelIm2.x;
	}
	else
	{
		addResult.x =0;
	}


	if((pixelIm1.y + pixelIm2.y)>=1) 
	{
		addResult.y = 1;
	}
	else if((pixelIm1.y + pixelIm2.y)>0 )
	{
		addResult.y =  pixelIm1.y + pixelIm2.y;
	}
	else
	{
		addResult.y =0;
	}

	if((pixelIm1.z + pixelIm2.z)>=1) 
	{
		addResult.z = 1;
	}
	else if((pixelIm1.z + pixelIm2.z)>0 )
	{
		addResult.z = pixelIm1.z + pixelIm2.z; 
	}
	else
	{
		addResult.z =0;
	}
	
	
	if(pixelIm1.w==0)
	{
		addResult.w = pixelIm2.w;
	}
	else if(pixelIm2.w==0)
	{
		addResult.w = pixelIm1.w;
	}
	else if(pixelIm2.w!=0 && pixelIm1.w!=0 )
	{
		addResult.w = 1;
	}
	else
	{
		addResult.w=0;
	}




	
	write_pixel_F4(output, gID_X, gID_Y, addResult, imageWidth, imageHeight);
 }