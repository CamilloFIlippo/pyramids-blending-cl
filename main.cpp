#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>

#include <CL/cl.h>
#include <FreeImage.h>
#include "ImagesCL.h"



using namespace std;

cl_mem maskBuffer;
cl_mem filter_cl;


const unsigned int PYRAMID_SIZE = 4;


#define FREE(ptr, free_val) \
if (ptr != free_val)    \
{                   \
	free(ptr);      \
	ptr = free_val; \
}



void InitCL(const char* filePath)
{
	cl_int resultCL;

	clHandles.context = NULL;
	clHandles.devices = NULL;
	clHandles.program = NULL;
	clHandles.commandQueue = NULL;

	size_t deviceListSize;

	/////////////////////////////////////////////////////////////////
	// Find the available platforms and select one
	/////////////////////////////////////////////////////////////////
	cl_uint numPlatforms;
	cl_platform_id targetPlatform = NULL;

	resultCL = clGetPlatformIDs(0, NULL, &numPlatforms);
	if (resultCL != CL_SUCCESS)
		throw (string("InitCL()::Error: Getting number of platforms (clGetPlatformIDs)"));

	if (!(numPlatforms > 0))
		throw (string("InitCL()::Error: No platforms found (clGetPlatformIDs)"));

	cl_platform_id* allPlatforms = (cl_platform_id*)malloc(numPlatforms * sizeof(cl_platform_id));

	resultCL = clGetPlatformIDs(numPlatforms, allPlatforms, NULL);
	if (resultCL != CL_SUCCESS)
		throw (string("InitCL()::Error: Getting platform ids (clGetPlatformIDs)"));

	// Num of platforms

	/* Select the target platform. Default: first platform */

	for (int i = 0; i < numPlatforms; i++)
	{
		char pbuff[128];
		resultCL = clGetPlatformInfo(allPlatforms[i],
			CL_PLATFORM_VENDOR,
			sizeof(pbuff),
			pbuff,
			NULL);
		if (resultCL != CL_SUCCESS)
			throw (string("InitCL()::Error: Getting platform info (clGetPlatformInfo)"));

		if (!strcmp(pbuff, "Advanced Micro Devices, Inc."))
		{
			targetPlatform = allPlatforms[i];
			break;
		}
	}


	targetPlatform = allPlatforms[0];

	FREE(allPlatforms, NULL);

	/////////////////////////////////////////////////////////////////
	// Create an OpenCL context
	/////////////////////////////////////////////////////////////////

	// Get all available platforms

	cl_context_properties cprops[3] = { CL_CONTEXT_PLATFORM, (cl_context_properties)targetPlatform, 0 };
	clHandles.context = clCreateContextFromType(cprops,
		CL_DEVICE_TYPE_GPU,
		NULL,
		NULL,
		&resultCL);

	if ((resultCL != CL_SUCCESS) || (clHandles.context == NULL))
		throw (string("InitCL()::Error: Creating Context (clCreateContextFromType)"));

	/////////////////////////////////////////////////////////////////
	// Detect OpenCL devices
	/////////////////////////////////////////////////////////////////
	/* First, get the size of device list */
	resultCL = clGetContextInfo(clHandles.context,
		CL_CONTEXT_DEVICES,
		0,
		NULL,
		&deviceListSize);

	//deviceListSize = deviceListSize / sizeof(cl_device_id);

	if (resultCL != CL_SUCCESS)
		throw(string("InitCL()::Error:  Getting Context Info."));
	if (deviceListSize == 0)
		throw(string("InitCL()::Error: No devices found."));

	/* Now, allocate the device list */
	clHandles.devices = (cl_device_id *)malloc(deviceListSize);

	if (clHandles.devices == 0)
		throw(string("InitCL()::Error: Could not allocate memory."));

	/* Next, get the device list data */
	resultCL = clGetContextInfo(clHandles.context,
		CL_CONTEXT_DEVICES,
		deviceListSize,
		clHandles.devices,
		NULL);

	if (resultCL != CL_SUCCESS)
		throw(string("InitCL()::Error: Getting Context Info. (device list, clGetContextInfo)"));

	/////////////////////////////////////////////////////////////////
	// Create an OpenCL command queue
	/////////////////////////////////////////////////////////////////


	clHandles.commandQueue = clCreateCommandQueue(clHandles.context,
		clHandles.devices[0],
		0,
		&resultCL);

	if ((resultCL != CL_SUCCESS) || (clHandles.commandQueue == NULL))
		throw(string("InitCL()::Creating Command Queue. (clCreateCommandQueue)"));

	/////////////////////////////////////////////////////////////////
	// Load CL file, build CL program object, create CL kernel object
	/////////////////////////////////////////////////////////////////

	std::ifstream kernelFile(filePath, std::ios::in);	//open File for reading
	if (!kernelFile.is_open())
	{
		std::cerr << "Failed to open file for reading: " << filePath << std::endl;
		return;
	}

	std::ostringstream oss;
	oss << kernelFile.rdbuf();

	std::string srcStdStr = oss.str();
	const char *srcStr = srcStdStr.c_str();

	const char * src = srcStdStr.c_str();
	size_t length = srcStdStr.length();



	clHandles.program = clCreateProgramWithSource(clHandles.context,
		1,
		(const char**)&srcStr,
		&length,
		&resultCL);

	if ((resultCL != CL_SUCCESS) || (clHandles.program == NULL))
		throw(string("InitCL()::Error: Loading Binary into cl_program. (clCreateProgramWithBinary)"));


	int filterSize = 5;
	int width = 200;

	//sprintf(clHandles.compilerOptions, "-D IMAGE_H=%d -D FILTER_SIZE=%d -D HALF_FILTER_SIZE=%d -D TWICE_HALF_FILTER_SIZE=%d -D HALF_FILTER_SIZE_IMAGE_W=%d",
	//	width,
	//	width,
	//	filterSize,
	//	filterSize / 2,
	//	(filterSize / 2) * 2,
	//	(filterSize / 2) * 20
	//	);

	//clHandles.context->setCompilerOptions(clHandles.compilerOptions);

	resultCL = clBuildProgram(clHandles.program, 1, clHandles.devices, clHandles.compilerOptions, NULL, NULL);

	if ((resultCL != CL_SUCCESS) || (clHandles.program == NULL))
	{
		cerr << "InitCL()::Error: In clBuildProgram" << endl;

		size_t length;
		resultCL = clGetProgramBuildInfo(clHandles.program,
			clHandles.devices[0],
			CL_PROGRAM_BUILD_LOG,
			0,
			NULL,
			&length);
		if (resultCL != CL_SUCCESS)
			throw(string("InitCL()::Error: Getting Program build info(clGetProgramBuildInfo)"));

		char* buffer = (char*)malloc(length);
		resultCL = clGetProgramBuildInfo(clHandles.program,
			clHandles.devices[0],
			CL_PROGRAM_BUILD_LOG,
			length,
			buffer,
			NULL);
		if (resultCL != CL_SUCCESS)
			throw(string("InitCL()::Error: Getting Program build info(clGetProgramBuildInfo)"));

		cerr << buffer << endl;
		free(buffer);

		throw(string("InitCL()::Error: Building Program (clBuildProgram)"));
	}

}

void Cleanup(cl_context context, cl_command_queue commandQueue, cl_program program, vector<cl_kernel> kernel)
{
	if (commandQueue != 0)
		clReleaseCommandQueue(commandQueue);

	if (program != 0)
		clReleaseProgram(program);

	if (context != 0)
		clReleaseContext(context);
}


////--------------------------------------------------------------------------------------------------------------------------////
////----------------------------------------- IMAGE CALCULATIONS -------------------------------------------------------------////


void PyramidDown(ImageBuffer &in, ImageBuffer &outBuf, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 0)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outBuf.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &maskBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &in.imageInfo);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_mem), &outBuf.imageInfo);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &extendMaskHeight);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 6, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 7, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { width, height };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}


void PyramidDown4F(ImageBuffer &in, ImageBuffer &outBuf, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 11)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outBuf.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &filter_cl);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_float4)* (localWorkSize[0] + 4) * (localWorkSize[1] + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}


void PyramidDownROI(ImageBuffer &in, ImageBuffer &outBuf, ImageBuffer gaussian, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 23)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outBuf.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &filter_cl);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &gaussian.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_float4)* (localWorkSize[0] + 4) * (localWorkSize[1] + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 6, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}


void PyramidUpROI(ImageBuffer &in, ImageBuffer &outBuf, ImageBuffer gaussian, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 24)
{

	//	size_t workitem_size[3];
	//	clGetDeviceInfo(*clHandles.devices, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(workitem_size), &workitem_size, NULL);
	//	printf("  CL_DEVICE_MAX_WORK_ITEM_SIZES:\t%u / %u / %u \n", workitem_size[0], workitem_size[1], workitem_size[2]);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);




	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);


	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 4;
	}

	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outBuf.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &filter_cl);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &gaussian.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_float4)* (localWorkSize[0] * 2 + 4) * (localWorkSize[1] * 2 + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 6, sizeof(cl_uint), &height);


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }



	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}



void MatrixMultiplication(ImageBuffer &in, ImageBuffer&in2, ImageBuffer &outBuf, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 16)
{

	//clock_t begin = clock();

	cl_event eventM;

	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &in2.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &outBuf.imageData);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &width);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &height);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_float4) * width, NULL);


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width*height, height };

	//	localWorkSize[0] = (size_t)128;
	//	globalWorkSize[0] = (size_t)width;

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		1,
		NULL,
		globalWorkSize,
		NULL,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	//clWaitForEvents(1, &eventM);


	//clock_t end = clock();
	///double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	//cout << elapsed_secs << endl;

}

void LogOut(Image &in, int length = 0, int type = 0)
{

	if (length == 0)
		length = in.imageInfo.channels * in.imageInfo.width * in.imageInfo.height;
	else
		length = length * 4;




	if (type == 0)
	{
		for (int i = 0; i <length; i += 4)
		{
			std::cout << "R :" << int(in.imageShort[i]) << " ";
			std::cout << "G :" << int(in.imageShort[i + 1]) << " ";
			std::cout << "B :" << int(in.imageShort[i + 2]) << " ";
			std::cout << "A :" << int(in.imageShort[i + 3]) << endl;
		}
	}
	else if (type == 1)
	{
		for (int i = 0; i <length; i += 4)
		{
			std::cout << "R :" << float(in.imageFloat[i]) << " ";
			std::cout << "G :" << float(in.imageFloat[i + 1]) << " ";
			std::cout << "B :" << float(in.imageFloat[i + 2]) << " ";
			std::cout << "A :" << float(in.imageFloat[i + 3]) << endl;
		}

	}
	else if (type == 2)
	{

		length = in.imageInfo.width * in.imageInfo.height;



		for (int i = 0; i <length; i++)
		{
			std::cout << "R :" << in.imageFloat4[i].s[0] << " ";
			std::cout << "G :" << in.imageFloat4[i].s[1] << " ";
			std::cout << "B :" << in.imageFloat4[i].s[2] << " ";
			std::cout << "A :" << in.imageFloat4[i].s[3] << endl;
		}

	}

	else
	{
		for (int i = 0; i <length; i += 4)
		{
			std::cout << "R :" << int(in.imageBytes[i]) << " ";
			std::cout << "G :" << int(in.imageBytes[i + 1]) << " ";
			std::cout << "B :" << int(in.imageBytes[i + 2]) << " ";
			std::cout << "A :" << int(in.imageBytes[i + 3]) << endl;
		}

	}


}


void SubtractImages(ImageBuffer &in, ImageBuffer&in2, ImageBuffer &outBuf, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 13)
{
	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);

	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = workitem_size / 2;
	}



	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width*height, height };

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &in2.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &outBuf.imageData);
	//	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &width);
	//	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		1,
		NULL,
		globalWorkSize,
		NULL,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}


void AddImages(ImageBuffer &in, ImageBuffer&in2, ImageBuffer &outBuf, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 17)
{
	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);

	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}



	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width * height, height };

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &in2.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &outBuf.imageData);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &width);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		1,
		NULL,
		globalWorkSize,
		NULL,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}

void AddImagesBlending(ImageBuffer &in, ImageBuffer&in2, ImageBuffer &outBuf, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 18)
{
	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);

	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}

	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width*height, height };

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &in2.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &outBuf.imageData);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &width);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		1,
		NULL,
		globalWorkSize,
		NULL,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}


void PyramidUp4F(ImageBuffer &in, ImageBuffer &outBuf, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 12)
{

	//	size_t workitem_size[3];
	//	clGetDeviceInfo(*clHandles.devices, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(workitem_size), &workitem_size, NULL);
	//	printf("  CL_DEVICE_MAX_WORK_ITEM_SIZES:\t%u / %u / %u \n", workitem_size[0], workitem_size[1], workitem_size[2]);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);




	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);


	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 4;
	}

	size_t localWorkSize[2] = { workitem_size, workitem_size };
	size_t globalWorkSize[2] = { width, height };




	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outBuf.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &filter_cl);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_float4)* (localWorkSize[0] * 2 + 4) * (localWorkSize[1] * 2 + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &height);


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }



	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }





}


void PyramidDownTest(ImageBuffer &in, ImageBuffer &outBuf, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, cl_uint kernelID = 5)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);



	maskBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_float)* extendMaskHeight* extendMaskHeight,
		static_cast<void *>(extendMaskInvert),
		&errNum);


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outBuf.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &maskBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &in.imageInfo);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_mem), &outBuf.imageInfo);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &extendMaskHeight);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 6, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 7, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { width, height };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}



void PyramidUp(ImageBuffer &imIn, ImageBuffer &out, cl_uint2 size, cl_event* imageEventIn = NULL, cl_event* imageEventOut = NULL, cl_uint kernelID = 1)
{
	cl_int errNum;

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &imIn.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &maskBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &imIn.imageInfo);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_mem), &out.imageInfo);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &extendMaskWidth);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 6, sizeof(cl_uint), &size.s[0]);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 7, sizeof(cl_uint), &size.s[1]);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { size.s[0], size.s[1] };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		1,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}


void GetRoi(ImageBuffer &imIn, ImageBuffer &mask, ImageBuffer &out, cl_uint2 size, cl_event* imageEventIn = NULL, cl_event* imageEventOut = NULL, cl_uint kernelID = 28)
{
	cl_int errNum;

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &imIn.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &mask.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &size.s[0]);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &size.s[1]);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { size.s[0], size.s[1] };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		0,
		0);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}


void Braka(ImageBuffer &in,  ImageBuffer &out, cl_uint2 size, cl_event* imageEventIn = NULL, cl_event* imageEventOut = NULL, cl_uint kernelID = 36)
{
	cl_int errNum;

	int a = 0;
	int b = 0;
	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(int), &a);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(int), &b);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &out.imageData);


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	

	/*
	size_t localWorkSize[1] = {256};
	size_t globalWorkSize[1] = { size.s[0] * size.s[1] * 4};

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		1,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		0,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }
	*/

	
	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { size.s[0], size.s[1] };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		0,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }
	

}


void SetBrightness(ImageBuffer &imIn, ImageBuffer &mask, ImageBuffer &out, int brightness, cl_uint2 size, cl_event* imageEventIn = NULL, cl_event* imageEventOut = NULL, cl_uint kernelID = 29)
{
	cl_int errNum;

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &imIn.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &mask.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &brightness);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &size.s[0]);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &size.s[1]);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { size.s[0], size.s[1] };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		0,
		0);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}


void Histogram(ImageBuffer &imIn, cl_uint2 size, int &histAvarage, cl_event* imageEventIn = NULL, cl_event* imageEventOut = NULL, cl_uint kernelID = 26)
{
	cl_int errNum;

	int num_pixels_per_work_item = 32;

	size_t  gsize[2];
	int     w;

	size_t workgroup_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), clHandles.devices[0], CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &workgroup_size, NULL);

	if (workgroup_size <= 256)
	{
		gsize[0] = 16;
		gsize[1] = workgroup_size / 16;
	}
	else if (workgroup_size <= 1024)
	{
		gsize[0] = workgroup_size / 16;
		gsize[1] = 16;
	}
	else
	{
		gsize[0] = workgroup_size / 32;
		gsize[1] = 32;
	}

	size_t localWorkSize[2];
	size_t globalWorkSize[2];
	size_t partialLocalWorkSize[2];
	size_t partialGlobalWorkSize[2];
	partialGlobalWorkSize[0] = 256 * 3;
	partialLocalWorkSize[0] = (workgroup_size > 256) ? 256 : workgroup_size;
	localWorkSize[0] = gsize[0];
	localWorkSize[1] = gsize[1];

	w = (size.s[0] + num_pixels_per_work_item - 1) / num_pixels_per_work_item;
	globalWorkSize[0] = ((w + gsize[0] - 1) / gsize[0]);
	globalWorkSize[1] = ((size.s[1] + gsize[1] - 1) / gsize[1]);

	size_t num_groups = globalWorkSize[0] * globalWorkSize[1];
	globalWorkSize[0] *= gsize[0];
	globalWorkSize[1] *= gsize[1];

	cl_mem partial_histogram_buffer = clCreateBuffer(clHandles.context, CL_MEM_READ_WRITE, num_groups * 257 * 3 * sizeof(unsigned int), NULL, NULL);
	cl_mem histogram_buffer = clCreateBuffer(clHandles.context, CL_MEM_WRITE_ONLY, 257 * 3 * sizeof(unsigned int), NULL, NULL);

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &imIn.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(int), &num_pixels_per_work_item);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &partial_histogram_buffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_int), &size.s[0]);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_int), &size.s[1]);
	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID + 1), 0, sizeof(cl_mem), &partial_histogram_buffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID + 1), 1, sizeof(int), &num_groups);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID + 1), 2, sizeof(cl_mem), &histogram_buffer);
	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	errNum = clEnqueueNDRangeKernel(clHandles.commandQueue, clHandles.kernel.at(kernelID), 2, 0, globalWorkSize, localWorkSize, 0, 0, 0);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	errNum = clEnqueueNDRangeKernel(clHandles.commandQueue, clHandles.kernel.at(kernelID + 1), 1, NULL, partialGlobalWorkSize, partialLocalWorkSize, 0, NULL, NULL);
	std::vector<unsigned int> histogram_results;
	histogram_results.resize(257 * 3);

	errNum = clEnqueueReadBuffer(clHandles.commandQueue, histogram_buffer, CL_TRUE, 0, 257 * 3 * sizeof(unsigned int), histogram_results.data(), 0, NULL, NULL);

	long histogramAvarage = 0;
	long histogramSum = 0;

	histogram_results[256 * 3 + 2] = 0;
	for (int i = 0; i <257; i++)
	{
		histogramAvarage += histogram_results[i] * i;
		histogramAvarage += histogram_results[i + 257] * i;
		histogramAvarage += histogram_results[i + 514] * i;
		if (i == 0) continue;

		if (histogram_results[i] != 0 || histogram_results[i + 257] != 0 || histogram_results[i + 514] != 0)
			histogramSum += histogram_results[i] * 3;
	}

	histAvarage = histogramAvarage / ((float)histogramSum);
}

////--------------------------------------------------------------------------------------------------------------------------////

ImageBuffer createImageBuffer(void * hostPtrSource, void * hostPtrInfo, cl_mem_flags memoryFlagData, cl_mem_flags memoryFlagInfo, cl_uint2 size, cl_uint bufferSize = 0)
{
	cl_int errNum;


	bufferSize = bufferSize * size.s[0] * size.s[1];


	cl_mem imageDataBuffer = clCreateBuffer(clHandles.context,
		memoryFlagData,
		bufferSize,
		hostPtrSource,
		&errNum);
	if (errNum != CL_SUCCESS) { std::cerr << "cannot create buffer" << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	cl_mem imageInfoBuffer = clCreateBuffer(clHandles.context,
		memoryFlagInfo,
		sizeof(ImageInfo),
		hostPtrInfo,
		&errNum);
	if (errNum != CL_SUCCESS) { std::cerr << "cannot create buffer" << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	return ImageBuffer(imageDataBuffer, imageInfoBuffer);
}


ImageBuffer createImageBuffer(cl_uint2 size, cl_uint bufferSize)
{
	cl_int errNum;


	bufferSize = bufferSize * size.s[0] * size.s[1];



	cl_mem imageDataBuffer = clCreateBuffer(clHandles.context,
		CL_MEM_READ_WRITE,
		bufferSize,
		NULL,
		&errNum);
	if (errNum != CL_SUCCESS) { std::cerr << "cannot create buffer" << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	cl_mem imageInfoBuffer = clCreateBuffer(clHandles.context,
		CL_MEM_READ_WRITE,
		sizeof(ImageInfo),
		NULL,
		&errNum);
	if (errNum != CL_SUCCESS) { std::cerr << "cannot create buffer" << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	return ImageBuffer(imageDataBuffer, imageInfoBuffer);

}

void ReadBuffer(ImageBuffer &outbf, Image &out, cl_uint2 size, cl_uint channels = 4)
{
	cl_short * localOutputArray = new cl_short[size.s[0] * size.s[1] * channels];
	cl_int errNum;


	errNum = clEnqueueReadBuffer(clHandles.commandQueue,
		outbf.imageData,
		CL_TRUE,
		0,
		sizeof(cl_short)* size.s[0] * size.s[1] * channels,
		localOutputArray,
		0,
		NULL,
		NULL);



	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		outbf.imageInfo,
		CL_TRUE,
		0,
		sizeof(ImageInfo),
		&out.imageInfo,
		0,
		NULL,
		NULL);


	out.imageFloat = NULL;
	out.imageFloat4 = NULL;
	out.imageShort = localOutputArray;
	out.imageBytes = NULL;





	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }
}

void ReadBuffer4F(ImageBuffer &outbf, Image &out, cl_uint2 size, cl_uint channels = 4)
{
	cl_float4 * localOutputArray = new cl_float4[size.s[0] * size.s[1]];
	cl_int errNum;







	errNum = clEnqueueReadBuffer(clHandles.commandQueue,
		outbf.imageData,
		CL_TRUE,
		0,
		size.s[0] * size.s[1] * sizeof(cl_float4),
		localOutputArray,
		0,
		NULL,
		NULL);



	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	/*	errNum = clEnqueueReadBuffer(
	clHandles.commandQueue,
	outbf.imageInfo,
	CL_TRUE,
	0,
	sizeof(ImageInfo),
	&out.imageInfo,
	0,
	NULL,
	NULL);
	*/

	out.imageInfo.width = size.s[0];// size.s[0];
	out.imageInfo.height = size.s[1];// size.s[1];
	out.imageInfo.channels = 4;

	out.imageFloat = NULL;
	out.imageFloat4 = localOutputArray;
	out.imageShort = NULL;
	out.imageBytes = NULL;


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }
}

void InitKernels()
{
	vector<string> kernels;

	kernels.push_back("pyramid_down");	//0
	kernels.push_back("pyramid_up");	//1
	kernels.push_back("byte_to_short");	//2
	kernels.push_back("short_to_byte");	//3
	kernels.push_back("subtract_images");	//4
	kernels.push_back("pyramid_down_test"); //5
	kernels.push_back("float_to_byte");	//6
	kernels.push_back("byte_to_float");	//7
	kernels.push_back("float4_to_byte");	//8
	kernels.push_back("byte_to_float4");	//9
	kernels.push_back("test_kernel");	//10
	kernels.push_back("pyramid_down_4F");	//11
	kernels.push_back("pyramid_up_4F");	//12
	kernels.push_back("subtract_images_4F");//13
	kernels.push_back("build_mask");	//14
	kernels.push_back("resize_down");	//15
	kernels.push_back("matrix_multiplication"); //16
	kernels.push_back("add_images");	//17
	kernels.push_back("add_images_blending");//18
	kernels.push_back("reconstruct_mask");	//19
	kernels.push_back("extend_mask");	//20
	kernels.push_back("test_kernel2");	//21
	kernels.push_back("pyramid_down_local");	//22
	kernels.push_back("pyramid_down_ROI"); //23
	kernels.push_back("pyramid_up_ROI"); //24
	kernels.push_back("pyramid_up_local"); //25
	kernels.push_back("histogram_image"); //26
	kernels.push_back("histogram_sum"); //27
	kernels.push_back("get_roi"); //28
	kernels.push_back("set_brightness");//29
	kernels.push_back("float4_to_half"); //30
	kernels.push_back("half_to_float4"); //31
	kernels.push_back("convolution_half");//32
	kernels.push_back("pyramid_down_half");//33
	kernels.push_back("pyramid_up_half");//34
	kernels.push_back("subtract_images_half");//35
	kernels.push_back("MHCdemosaic");

	for (unsigned int i = 0; i< kernels.size(); i++)
	{
		clHandles.kernel.push_back(clCreateKernel(clHandles.program, kernels.at(i).c_str(), NULL));
		if (clHandles.kernel.at(0) == NULL)
		{
			std::cerr << "Failed to create kernel" << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel);
		}
	}
}

void ByteToShort(Image in, Image &out, int kernelID = 2)
{
	cl_int errNum;

	cl_mem inputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_uchar)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		in.imageBytes,
		&errNum);

	cl_mem outputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_WRITE,
		sizeof(cl_short)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		NULL,
		&errNum);

	cl_mem imageInfoBufferIn = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(in.imageInfo),
		&in.imageInfo,
		&errNum);

	cl_mem imageInfoBufferOut = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(out.imageInfo),
		&out.imageInfo,
		&errNum);


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &inputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &imageInfoBufferIn);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &imageInfoBufferOut);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { in.imageInfo.width, in.imageInfo.height };

	cl_double4 temp;

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	cl_short * localOutputArray = new cl_short[in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels * sizeof(cl_short)];

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		imageInfoBufferOut,
		CL_TRUE,
		0,
		sizeof(ImageInfo),
		&out.imageInfo,
		0,
		NULL,
		NULL);

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		outputSignalBuffer,
		CL_TRUE,
		0,
		in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels * sizeof(cl_short),
		localOutputArray,
		0,
		NULL,
		NULL);

	out.imageShort = localOutputArray;
	out.imageBytes = NULL;
	out.imageFloat = NULL;
	out.imageFloat4 = NULL;
}

void ByteToFloat(Image in, Image &out, int kernelID = 7)
{
	cl_int errNum;

	cl_mem inputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_uchar)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		in.imageBytes,
		&errNum);

	cl_mem outputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_WRITE,
		sizeof(cl_float)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		NULL,
		&errNum);

	cl_mem imageInfoBufferIn = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(in.imageInfo),
		&in.imageInfo,
		&errNum);

	cl_mem imageInfoBufferOut = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(out.imageInfo),
		&out.imageInfo,
		&errNum);


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &inputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &imageInfoBufferIn);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &imageInfoBufferOut);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { in.imageInfo.width, in.imageInfo.height };



	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	cl_float * localOutputArray = new cl_float[in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels * sizeof(cl_float)];

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		imageInfoBufferOut,
		CL_TRUE,
		0,
		sizeof(ImageInfo),
		&out.imageInfo,
		0,
		NULL,
		NULL);

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		outputSignalBuffer,
		CL_TRUE,
		0,
		in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels * sizeof(cl_float),
		localOutputArray,
		0,
		NULL,
		NULL);

	out.imageFloat = localOutputArray;
	out.imageShort = NULL;
	out.imageBytes = NULL;
}

void ByteToFloat4(Image in, Image &out, int kernelID = 9)
{
	cl_int errNum;

	cl_mem inputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_uchar)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		in.imageBytes,
		&errNum);

	cl_mem outputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_WRITE,
		sizeof(cl_float4)* in.imageInfo.width* in.imageInfo.height,
		NULL,
		&errNum);

	cl_mem imageInfoBufferIn = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(in.imageInfo),
		&in.imageInfo,
		&errNum);

	cl_mem imageInfoBufferOut = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(out.imageInfo),
		&out.imageInfo,
		&errNum);


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &inputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &imageInfoBufferIn);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &imageInfoBufferOut);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { in.imageInfo.width, in.imageInfo.height };



	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	cl_float4 * localOutputArray = new cl_float4[in.imageInfo.width* in.imageInfo.height];

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		imageInfoBufferOut,
		CL_TRUE,
		0,
		sizeof(ImageInfo),
		&out.imageInfo,
		0,
		NULL,
		NULL);

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		outputSignalBuffer,
		CL_TRUE,
		0,
		in.imageInfo.width* in.imageInfo.height * sizeof(cl_float4),
		localOutputArray,
		0,
		NULL,
		NULL);

	out.imageFloat4 = localOutputArray;
	out.imageFloat = NULL;
	out.imageShort = NULL;
	out.imageBytes = NULL;
}


float fix16tof(int n)
{
	float s = 1.0f;
	if (n < 0) {
		s = -1.0f;
		n = -n;
	}
	return s * ((float)(n >> 4) + ((n & 15) / 16.0f));
}

void ByteToHalf4(Image in, Image &out, int kernelID = 30)
{
	cl_int errNum;

	cl_mem inputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_uchar)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		in.imageBytes,
		&errNum);

	cl_mem outputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_WRITE,
		sizeof(cl_half)* in.imageInfo.width* in.imageInfo.height,
		NULL,
		&errNum);

	cl_mem imageInfoBufferIn = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(in.imageInfo),
		&in.imageInfo,
		&errNum);

	cl_mem imageInfoBufferOut = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(out.imageInfo),
		&out.imageInfo,
		&errNum);


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &inputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &imageInfoBufferIn);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &imageInfoBufferOut);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	in.imageInfo.width = 10;
	in.imageInfo.height = 10;

	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { in.imageInfo.width, in.imageInfo.height };



	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	std::vector<cl_half> localOutputArray;
	localOutputArray.resize(in.imageInfo.width* in.imageInfo.height);

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		imageInfoBufferOut,
		CL_TRUE,
		0,
		sizeof(ImageInfo),
		&out.imageInfo,
		0,
		NULL,
		NULL);

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		outputSignalBuffer,
		CL_TRUE,
		0,
		localOutputArray.size(),
		localOutputArray.data(),
		0,
		NULL,
		NULL);


	int  a = fix16tof(localOutputArray[0]);

}








void HalfToFloat4(ImageBuffer in, ImageBuffer &out, cl_uint2 size, int kernelID = 31)
{
	cl_int errNum;



	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(int), &size.s[0]);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(int), &size.s[1]);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { size.s[0], size.s[1] };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }



	/*	cl_float4 * localOutputArray = new cl_float4[size.s[0] , size.s[1]];

	//std::vector<cl_float4> localOutputArray;
	//localOutputArray.resize(in.imageInfo.width* in.imageInfo.height* 4);

	errNum = clEnqueueReadBuffer(
	clHandles.commandQueue,
	imageInfoBufferOut,
	CL_TRUE,
	0,
	sizeof(ImageInfo),
	&out.imageInfo,
	0,
	NULL,
	NULL);

	errNum = clEnqueueReadBuffer(
	clHandles.commandQueue,
	outputSignalBuffer,
	CL_TRUE,
	0,
	//localOutputArray.size(),
	size.s[0] * size.s[1] * sizeof(cl_float4),
	localOutputArray,
	0,
	NULL,
	NULL);


	out.imageInfo = in.imageInfo;
	out.imageFloat4 = localOutputArray;
	out.imageFloat = NULL;
	out.imageShort = NULL;
	out.imageBytes = NULL;


	//LogOut(out, 0 , 2);
	*/
}



void ConvolutionHalf(ImageBuffer in, ImageBuffer &out, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, int kernelID = 32)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 16;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_half)* (localWorkSize[0] + 4) * (localWorkSize[1] + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}


void PyramidDownHalf(ImageBuffer in, ImageBuffer &out, ImageBuffer &gaussian, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, int kernelID = 33)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 16;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &gaussian);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}


void SubtractHalf(ImageBuffer in, ImageBuffer in2, ImageBuffer &out, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, int kernelID = 35)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 16;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &in2.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &out.imageData);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &width);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		1,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}



void PyramidUpHalf(ImageBuffer in, ImageBuffer &out, ImageBuffer &gaussian, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, int kernelID = 34)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 16;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &gaussian);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}


void TestKernel(ImageBuffer in, ImageBuffer &out, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, int kernelID = 10)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 16;
	}


	size_t localWorkSize[2] = { workitem_size, workitem_size };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &filter_cl);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_float4)* (localWorkSize[0] + 4) * (localWorkSize[1] + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}

void PyramidDownLocal(ImageBuffer in, ImageBuffer &out, ImageBuffer gaussian, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, int kernelID = 22)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 16;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &gaussian.imageData);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &filter_cl);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_float4)* (localWorkSize[0] + 4) * (localWorkSize[1] + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}

void PyramidUpLocal(ImageBuffer &in, ImageBuffer &out, ImageBuffer &gaussian, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, int kernelID = 25)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 16;
	}


	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &gaussian.imageData);
	//errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &filter_cl);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_float4)* (localWorkSize[0] + 4) * (localWorkSize[1] + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}


void TestKernel2(ImageBuffer in, ImageBuffer &out, cl_uint2 size, cl_event* imageEventIn = 0, cl_event* imageEventOut = 0, int kernelID = 21)
{
	cl_int errNum;
	int width = size.s[0];
	int height = size.s[1];

	cl_int number_of_events = (imageEventIn != 0 ? 1 : 0);

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);



	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}
	else
	{
		workitem_size = 16;// workitem_size / 4;
	}


	size_t localWorkSize[2] = { workitem_size, workitem_size };
	size_t globalWorkSize[2] = { width, height };


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &filter_cl);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_float4)* (localWorkSize[0] + 4) * (localWorkSize[1] + 4) * 2, 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 5, sizeof(cl_uint), &height);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		number_of_events,
		imageEventIn,
		imageEventOut);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}


void ShortToByte(Image in, Image &out, int kernelID = 3)
{
	cl_int errNum;

	cl_mem inputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_short)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		in.imageShort,
		&errNum);

	cl_mem outputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_WRITE,
		sizeof(cl_uchar)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		NULL,
		&errNum);

	cl_mem imageInfoBufferIn = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(in.imageInfo),
		&in.imageInfo,
		&errNum);

	cl_mem imageInfoBufferOut = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(out.imageInfo),
		&out.imageInfo,
		&errNum);


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &inputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &imageInfoBufferIn);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &imageInfoBufferOut);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { in.imageInfo.width, in.imageInfo.height };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	cl_uchar * localOutputArray = new cl_uchar[in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels * sizeof(cl_uchar)];

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		imageInfoBufferOut,
		CL_TRUE,
		0,
		sizeof(ImageInfo),
		&out.imageInfo,
		0,
		NULL,
		NULL);

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		outputSignalBuffer,
		CL_TRUE,
		0,
		sizeof(cl_uchar)* out.imageInfo.channels * out.imageInfo.height * out.imageInfo.width,
		localOutputArray,
		0,
		NULL,
		NULL);

	out.imageBytes = localOutputArray;
	out.imageShort = NULL;
}

void Float4ToByte(Image in, Image &out, int kernelID = 8)
{
	cl_int errNum;

	cl_mem inputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_float4)* in.imageInfo.width* in.imageInfo.height,
		in.imageFloat4,
		&errNum);

	cl_mem outputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_WRITE,
		sizeof(cl_uchar)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		NULL,
		&errNum);

	cl_mem imageInfoBufferIn = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(in.imageInfo),
		&in.imageInfo,
		&errNum);

	cl_mem imageInfoBufferOut = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(out.imageInfo),
		&out.imageInfo,
		&errNum);


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &inputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &imageInfoBufferIn);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &imageInfoBufferOut);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { in.imageInfo.width, in.imageInfo.height };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	cl_uchar * localOutputArray = new cl_uchar[in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels * sizeof(cl_uchar)];

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		imageInfoBufferOut,
		CL_TRUE,
		0,
		sizeof(ImageInfo),
		&out.imageInfo,
		0,
		NULL,
		NULL);

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		outputSignalBuffer,
		CL_TRUE,
		0,
		sizeof(cl_uchar)* out.imageInfo.channels * out.imageInfo.height * out.imageInfo.width,
		localOutputArray,
		0,
		NULL,
		NULL);

	out.imageFloat = NULL;
	out.imageBytes = localOutputArray;
	out.imageShort = NULL;
}

void FloatToByte(Image in, Image &out, int kernelID = 6)
{
	cl_int errNum;

	cl_mem inputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_float)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		in.imageFloat,
		&errNum);

	cl_mem outputSignalBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_WRITE,
		sizeof(cl_uchar)* in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels,
		NULL,
		&errNum);

	cl_mem imageInfoBufferIn = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(in.imageInfo),
		&in.imageInfo,
		&errNum);

	cl_mem imageInfoBufferOut = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(out.imageInfo),
		&out.imageInfo,
		&errNum);


	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &inputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &outputSignalBuffer);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &imageInfoBufferIn);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_mem), &imageInfoBufferOut);

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { in.imageInfo.width, in.imageInfo.height };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	cl_uchar * localOutputArray = new cl_uchar[in.imageInfo.width* in.imageInfo.height * in.imageInfo.channels * sizeof(cl_uchar)];

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		imageInfoBufferOut,
		CL_TRUE,
		0,
		sizeof(ImageInfo),
		&out.imageInfo,
		0,
		NULL,
		NULL);

	errNum = clEnqueueReadBuffer(
		clHandles.commandQueue,
		outputSignalBuffer,
		CL_TRUE,
		0,
		sizeof(cl_uchar)* out.imageInfo.channels * out.imageInfo.height * out.imageInfo.width,
		localOutputArray,
		0,
		NULL,
		NULL);

	out.imageFloat = NULL;
	out.imageBytes = localOutputArray;
	out.imageShort = NULL;
}

void LoadImage(Image &image, const char * imagePath, int TypeFlag = 0) // 0 - short, 1 - float, 2 - float4 ,3 - byte
{
	FREE_IMAGE_FORMAT format = FreeImage_GetFileType(imagePath, 0);
	FIBITMAP* imageFI = FreeImage_Load(format, imagePath);



	FIBITMAP* temp = imageFI;
	//imageFI = FreeImage_ConvertTo24Bits(imageFI);
	int i = FreeImage_GetTransparentIndex(imageFI);


	image.imageInfo.width = FreeImage_GetWidth(imageFI);
	image.imageInfo.height = FreeImage_GetHeight(imageFI);
	image.imageInfo.channels = 4;
	image.imageInfo.pixelFormat = RGB;        // do sth with that
	int test = FreeImage_GetPitch(imageFI);
	FREE_IMAGE_COLOR_TYPE type = FreeImage_GetColorType(imageFI);


	cl_uchar * pixels = (cl_uchar*)FreeImage_GetBits(imageFI);

	cl_uchar *localBuffer = new cl_uchar[image.imageInfo.width * image.imageInfo.height * image.imageInfo.channels];
	memcpy(localBuffer, FreeImage_GetBits(imageFI), image.imageInfo.width * image.imageInfo.height * image.imageInfo.channels);

	for (int pix = 0; pix<FreeImage_GetWidth(imageFI) * FreeImage_GetHeight(imageFI); pix++)
	{
		localBuffer[pix * 4 + 0] = pixels[pix * 4 + 0];
		localBuffer[pix * 4 + 1] = pixels[pix * 4 + 1];
		localBuffer[pix * 4 + 2] = pixels[pix * 4 + 2];
		localBuffer[pix * 4 + 3] = pixels[pix * 4 + 3];
	}



	if (TypeFlag == 0)
	{
		image.imageBytes = localBuffer;
		ByteToShort(image, image);
	}
	else if (TypeFlag == 1)
	{
		image.imageBytes = localBuffer;
		ByteToFloat(image, image);
	}
	else if (TypeFlag == 2)
	{
		image.imageBytes = localBuffer;
		ByteToFloat4(image, image);
	}
	else if (TypeFlag == 3)
	{
		image.imageBytes = localBuffer;
		ByteToHalf4(image, image);
	}
	else
	{
		image.imageBytes = localBuffer;
	}


	FreeImage_Unload(temp);
	//	FreeImage_Unload(imageFI);
}

void SaveImage(Image image, const char *imagePath)	//TO REPLACE
{
	FREE_IMAGE_FORMAT format = FreeImage_GetFIFFromFilename(imagePath);

	/*if (image.imageBytes == NULL && image.imageFloat == NULL && image.imageFloat4 == NULL)
	ShortToByte(image, image);

	else if (image.imageBytes == NULL && image.imageShort == NULL && image.imageFloat4 == NULL)
	FloatToByte(image, image);

	else if (image.imageBytes == NULL && image.imageShort == NULL && image.imageFloat == NULL)
	Float4ToByte(image, image);
	*/


	FIBITMAP *imageFT = FreeImage_ConvertFromRawBits((BYTE*)image.imageBytes, image.imageInfo.width,
		image.imageInfo.height, image.imageInfo.width * image.imageInfo.channels, 32,
		0xFF000000, 0x00FF0000, 0x0000FF00);

	FreeImage_Save(format, imageFT, imagePath);
}

void CalculatePyramidDimensions(Image src, vector<cl_uint2> &pyramidsDimensions)
{
	int sizeX;
	int sizeY;

	cl_uint2 dimensions = { src.imageInfo.width, src.imageInfo.height };

	pyramidsDimensions.push_back(dimensions);



	for (int i = 0; i < PYRAMID_SIZE; i++)
	{
		sizeX = dimensions.s[0] / 2;
		sizeY = dimensions.s[1] / 2;

		if (!(sizeX % 2) == 0)
			sizeX = sizeX - 1;


		if (!(sizeY % 2) == 0)
			sizeY = sizeY - 1;


		dimensions.s[0] = sizeX;
		dimensions.s[1] = sizeY;

		pyramidsDimensions.push_back(dimensions);
	}
}


void InitLaplacianBuffer(vector<LaplacianBuffer> & laplacianBuffer, vector <cl_uint2> dimensions)
{
	for (int i = 0; i < PYRAMID_SIZE; i++)
	{
		LaplacianBuffer buffers;
		buffers.pyramidDown = createImageBuffer(dimensions.at(i + 1), sizeof(cl_float4));
		buffers.pyramidUp = createImageBuffer(dimensions.at(i), sizeof(cl_float4));
		buffers.subtractResult = createImageBuffer(dimensions.at(i), sizeof(cl_float4));
		laplacianBuffer.push_back(buffers);
	}
}


void BuildLaplacianPyramid(vector<ImageBuffer> *pyramid,
	vector<LaplacianBuffer> laplacianBuffers,
	const ImageBuffer &image,
	const vector <cl_uint2> *pyramidsDimensions,
	ImageBuffer &smallestLevel,
	cl_event * inImageBuffer = 0)
{
	ImageBuffer currentImage = image;


	clock_t begin = clock();

	PyramidDown4F(currentImage, laplacianBuffers.at(0).pyramidDown, pyramidsDimensions->at(1), 0, &(laplacianBuffers.at(0).pyramidDown.imageEvent));
	PyramidUp4F(laplacianBuffers.at(0).pyramidDown, laplacianBuffers.at(0).pyramidUp, pyramidsDimensions->at(0), &(laplacianBuffers.at(0).pyramidDown.imageEvent), &(laplacianBuffers.at(0).pyramidUp.imageEvent));

	clWaitForEvents(1, &(laplacianBuffers.at(0).pyramidUp.imageEvent));

	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << "inside global " << elapsed_secs << endl;

	SubtractImages(currentImage, laplacianBuffers.at(0).pyramidUp, laplacianBuffers.at(0).subtractResult, pyramidsDimensions->at(0), &(laplacianBuffers.at(0).pyramidUp.imageEvent), &(laplacianBuffers.at(0).subtractResult.imageEvent));
	pyramid->push_back(laplacianBuffers.at(0).subtractResult);
	currentImage = laplacianBuffers.at(0).pyramidDown;

	for (int i = 1; i < PYRAMID_SIZE; i++)
	{
		PyramidDown4F(currentImage, laplacianBuffers.at(i).pyramidDown, pyramidsDimensions->at(i + 1), &(laplacianBuffers.at(i - 1).subtractResult.imageEvent), &(laplacianBuffers.at(i).pyramidDown.imageEvent));
		PyramidUp4F(laplacianBuffers.at(i).pyramidDown, laplacianBuffers.at(i).pyramidUp, pyramidsDimensions->at(i), &(laplacianBuffers.at(i).pyramidDown.imageEvent), &(laplacianBuffers.at(i).pyramidUp.imageEvent));
		SubtractImages(currentImage, laplacianBuffers.at(i).pyramidUp, laplacianBuffers.at(i).subtractResult, pyramidsDimensions->at(i), &(laplacianBuffers.at(i).pyramidUp.imageEvent), &(laplacianBuffers.at(i).subtractResult.imageEvent));
		pyramid->push_back(laplacianBuffers.at(i).subtractResult);
		currentImage = laplacianBuffers.at(i).pyramidDown;
	}
	//clWaitForEvents(1, &(laplacianBuffers.at(PYRAMID_SIZE - 1).subtractResult.imageEvent));

	smallestLevel = currentImage;

}



void BuildLaplacianPyramidROI(vector<ImageBuffer> *pyramid,
	vector<LaplacianBuffer> laplacianBuffers,
	const ImageBuffer &image,
	const vector <cl_uint2> *pyramidsDimensions,
	vector<ImageBuffer> *gaussian,
	ImageBuffer &smallestLevel,
	cl_event * inImageBuffer = 0)
{
	ImageBuffer currentImage = image;


	PyramidDownROI(currentImage, laplacianBuffers.at(0).pyramidDown, gaussian->at(1), pyramidsDimensions->at(1), inImageBuffer, &(laplacianBuffers.at(0).pyramidDown.imageEvent));
	PyramidUpROI(laplacianBuffers.at(0).pyramidDown, laplacianBuffers.at(0).pyramidUp, gaussian->at(1), pyramidsDimensions->at(0), &(laplacianBuffers.at(0).pyramidDown.imageEvent), &(laplacianBuffers.at(0).pyramidUp.imageEvent));
	SubtractImages(currentImage, laplacianBuffers.at(0).pyramidUp, laplacianBuffers.at(0).subtractResult, pyramidsDimensions->at(0), &(laplacianBuffers.at(0).pyramidUp.imageEvent), &(laplacianBuffers.at(0).subtractResult.imageEvent));
	pyramid->push_back(laplacianBuffers.at(0).subtractResult);
	currentImage = laplacianBuffers.at(0).pyramidDown;

	for (int i = 1; i < PYRAMID_SIZE; i++)
	{
		PyramidDownROI(currentImage, laplacianBuffers.at(i).pyramidDown, gaussian->at(i + 1), pyramidsDimensions->at(i + 1), &(laplacianBuffers.at(i - 1).subtractResult.imageEvent), &(laplacianBuffers.at(i).pyramidDown.imageEvent));
		PyramidUpROI(laplacianBuffers.at(i).pyramidDown, laplacianBuffers.at(i).pyramidUp, gaussian->at(i + 1), pyramidsDimensions->at(i), &(laplacianBuffers.at(i).pyramidDown.imageEvent), &(laplacianBuffers.at(i).pyramidUp.imageEvent));
		SubtractImages(currentImage, laplacianBuffers.at(i).pyramidUp, laplacianBuffers.at(i).subtractResult, pyramidsDimensions->at(i), &(laplacianBuffers.at(i).pyramidUp.imageEvent), &(laplacianBuffers.at(i).subtractResult.imageEvent));
		pyramid->push_back(laplacianBuffers.at(i).subtractResult);
		currentImage = laplacianBuffers.at(i).pyramidDown;
	}
	//clWaitForEvents(1, &(laplacianBuffers.at(PYRAMID_SIZE - 1).subtractResult.imageEvent));

	smallestLevel = currentImage;

}

void BuildLaplacianPyramidLocal(vector<ImageBuffer> *pyramid,
	vector<LaplacianBuffer> laplacianBuffers,
	const ImageBuffer &image,
	const vector <cl_uint2> *pyramidsDimensions,
	vector<ImageBuffer> *gaussian,
	ImageBuffer &smallestLevel,
	cl_event * inImageBuffer = 0)
{
	ImageBuffer currentImage = image;
	clFlush(clHandles.commandQueue);
	clock_t begin = clock();

	PyramidDownLocal(currentImage, laplacianBuffers.at(0).pyramidDown, gaussian->at(0), pyramidsDimensions->at(0), inImageBuffer, &(laplacianBuffers.at(0).pyramidDown.imageEvent));
	PyramidUpLocal(laplacianBuffers.at(0).pyramidDown, laplacianBuffers.at(0).pyramidUp, gaussian->at(1), pyramidsDimensions->at(0), &(laplacianBuffers.at(0).pyramidDown.imageEvent), &(laplacianBuffers.at(0).pyramidUp.imageEvent));
	SubtractImages(currentImage, laplacianBuffers.at(0).pyramidUp, laplacianBuffers.at(0).subtractResult, pyramidsDimensions->at(0), &(laplacianBuffers.at(0).pyramidUp.imageEvent), &(laplacianBuffers.at(0).subtractResult.imageEvent));

	clWaitForEvents(1, &(laplacianBuffers.at(0).subtractResult.imageEvent));

	clock_t end = clock();

	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << elapsed_secs << endl;

	pyramid->push_back(laplacianBuffers.at(0).subtractResult);
	currentImage = laplacianBuffers.at(0).pyramidDown;

	for (int i = 1; i < PYRAMID_SIZE; i++)
	{
		PyramidDownLocal(currentImage, laplacianBuffers.at(i).pyramidDown, gaussian->at(i), pyramidsDimensions->at(i), &(laplacianBuffers.at(i - 1).subtractResult.imageEvent), &(laplacianBuffers.at(i).pyramidDown.imageEvent));
		PyramidUpLocal(laplacianBuffers.at(i).pyramidDown, laplacianBuffers.at(i).pyramidUp, gaussian->at(i + 1), pyramidsDimensions->at(i), &(laplacianBuffers.at(i).pyramidDown.imageEvent), &(laplacianBuffers.at(i).pyramidUp.imageEvent));
		SubtractImages(currentImage, laplacianBuffers.at(i).pyramidUp, laplacianBuffers.at(i).subtractResult, pyramidsDimensions->at(i), &(laplacianBuffers.at(i).pyramidUp.imageEvent), &(laplacianBuffers.at(i).subtractResult.imageEvent));
		pyramid->push_back(laplacianBuffers.at(i).subtractResult);
		currentImage = laplacianBuffers.at(i).pyramidDown;
	}

	//clWaitForEvents(1, &(laplacianBuffers.at(PYRAMID_SIZE - 1).subtractResult.imageEvent));

	smallestLevel = currentImage;

}







void BuildLaplacianPyramid2(vector<ImageBuffer> *pyramid,
	vector<LaplacianBuffer> laplacianBuffers,
	const ImageBuffer &image,
	const vector <cl_uint2> *dimensions,
	ImageBuffer &smallestLevel,
	cl_event * inImageBuffer = 0)
{
	ImageBuffer currentImage = image;

	TestKernel(image, laplacianBuffers.at(0).pyramidDown, dimensions->at(0), 0, &laplacianBuffers.at(0).pyramidDown.imageEvent);
	TestKernel2(laplacianBuffers.at(0).pyramidDown, laplacianBuffers.at(0).pyramidUp, dimensions->at(0), &laplacianBuffers.at(0).pyramidDown.imageEvent, &laplacianBuffers.at(0).pyramidUp.imageEvent);
	SubtractImages(currentImage, laplacianBuffers.at(0).pyramidUp, laplacianBuffers.at(0).subtractResult, dimensions->at(0), &laplacianBuffers.at(0).pyramidUp.imageEvent, &laplacianBuffers.at(0).subtractResult.imageEvent);
	pyramid->push_back(laplacianBuffers.at(0).subtractResult);
	currentImage = laplacianBuffers.at(0).pyramidDown;
	clWaitForEvents(1, &laplacianBuffers.at(0).subtractResult.imageEvent);


	for (int i = 1; i < 2; i++)
	{
		TestKernel(currentImage, laplacianBuffers.at(i).pyramidDown, dimensions->at(i), &(laplacianBuffers.at(i - 1).subtractResult.imageEvent), &(laplacianBuffers.at(i).pyramidDown.imageEvent));
		pyramid->push_back(laplacianBuffers.at(1).pyramidDown);

		TestKernel2(laplacianBuffers.at(i).pyramidDown, laplacianBuffers.at(i).pyramidUp, dimensions->at(i), &(laplacianBuffers.at(i).pyramidDown.imageEvent), &(laplacianBuffers.at(i).pyramidUp.imageEvent));
		SubtractImages(currentImage, laplacianBuffers.at(i).pyramidUp, laplacianBuffers.at(i).subtractResult, dimensions->at(i), &(laplacianBuffers.at(i).pyramidUp.imageEvent), &(laplacianBuffers.at(i).subtractResult.imageEvent));
		pyramid->push_back(laplacianBuffers.at(i).subtractResult);
		currentImage = laplacianBuffers.at(i).pyramidDown;
	}

	//clWaitForEvents(1, &(laplacianBuffers.at(PYRAMID_SIZE-1).subtractResult.imageEvent));

	smallestLevel = currentImage;

}






void CreateLaplacianBuffers(vector<cl_uint2> dimensions, int numberOfImages, vector<vector<LaplacianBuffer>> &laplacianBuffers)
{
	for (int i = 0; i < numberOfImages; i++)
	{
		vector<LaplacianBuffer> lapBuf;
		InitLaplacianBuffer(lapBuf, dimensions);
		laplacianBuffers.push_back(lapBuf);
	}
}

void CreateLaplacianPyramids(vector<ImageBuffer> images,
	vector<cl_uint2> *dimensions,
	vector<vector<LaplacianBuffer>> laplacianBuffers,
	vector<ImageBuffer> &smallestLevels,
	vector<vector<ImageBuffer>> &lapPyramids)
{

	for (int i = 0; i< images.size(); i++)
	{
		ImageBuffer base;
		vector<ImageBuffer> *laplacian = NULL;
		BuildLaplacianPyramid(laplacian, laplacianBuffers.at(i), images.at(i), dimensions, base);
		smallestLevels.push_back(base);
		lapPyramids.push_back(*laplacian);
		clWaitForEvents(1, &laplacian->at(PYRAMID_SIZE - 1).imageEvent);
	}
}




float * filter;

int filterSize = 5;
float PI = 3.14;

void generateFilter()
{
	const float DELTA = 1.84089642f * ((float)filterSize / 7.0);
	const float TWO_DELTA_SQ = 2.0f * DELTA * DELTA;
	const float k = 1.0f / (PI * TWO_DELTA_SQ);


	filter = new float[filterSize * filterSize * 4];
	float * fP = filter;
	int w = filterSize / 2;

	const float * precomputed = NULL;


	precomputed = gaussian5;



	for (int r = -w; r <= w; r++)
	{
		for (int c = -w; c <= w; c++, fP += 4)
		{
			if (precomputed)
			{
				fP[0] = *precomputed;
				fP[1] = *precomputed;
				fP[2] = *precomputed;
				fP[3] = r == c && c == 0 ? 1.0f : 0.0f;

				precomputed++;
			}
			else
			{
				const float v = k * exp(-(r*r + c*c) / TWO_DELTA_SQ);
				fP[0] = v;
				fP[1] = v;
				fP[2] = v;
				fP[3] = r == c && c == 0 ? 1.0f : 0.0f;
			}
		}

	}
}

void InitBuffers()
{
	cl_int errNum;

	maskBuffer = clCreateBuffer(
		clHandles.context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_float)* extendMaskHeight* extendMaskHeight,
		static_cast<void *>(extendMask),
		&errNum);

	if (errNum != CL_SUCCESS) { std::cerr << "Error init the buffer." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	generateFilter();

	filter_cl = clCreateBuffer(clHandles.context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		filterSize * filterSize * sizeof(float) * 4, filter, &errNum);

	if (errNum != CL_SUCCESS) { std::cerr << "Error init the buffer." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }
}

void BuildMask(ImageBuffer in1, ImageBuffer in2, ImageBuffer &out, cl_uint2 size, cl_uint kernelID = 14)
{
	cl_int errNum;

	size_t workitem_size;
	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);

	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { size.s[0], size.s[1] };

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in1.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &in2.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &size.s[0]);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &size.s[1]);


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}

void ReconstructMask(ImageBuffer in, ImageBuffer &out, cl_uint2 size, cl_uint kernelID = 19)
{
	cl_int errNum;
	cl_uint width = size.s[0];
	cl_uint height = size.s[1];

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &height);


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { width, height };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


}

void ExtendMask(ImageBuffer in1, ImageBuffer in2, ImageBuffer &out, cl_uint2 size, cl_uint kernelID = 20)
{
	cl_int errNum;
	cl_uint width = size.s[0];
	cl_uint height = size.s[1];

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in1.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &in2.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &width);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &height);


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { width, height };

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		NULL);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

}

void ResizeDown(ImageBuffer in, ImageBuffer &out, cl_uint2 size, cl_uint kernelID = 15)
{
	cl_int errNum;

	size_t workitem_size;
	int width = size.s[0];
	int height = size.s[1];

	clGetKernelWorkGroupInfo(clHandles.kernel.at(kernelID), *clHandles.devices, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(workitem_size), &workitem_size, NULL);

	if (workitem_size >= width || workitem_size >= height)
	{
		workitem_size = 1;
	}


	size_t localWorkSize[2] = { 1, 1 };
	size_t globalWorkSize[2] = { size.s[0], size.s[1] };

	errNum = clSetKernelArg(clHandles.kernel.at(kernelID), 0, sizeof(cl_mem), &in.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 1, sizeof(cl_mem), &out.imageData);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 2, sizeof(cl_float4)* (localWorkSize[0] + 4) * (localWorkSize[1] + 4), 0);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 3, sizeof(cl_uint), &size.s[0]);
	errNum |= clSetKernelArg(clHandles.kernel.at(kernelID), 4, sizeof(cl_uint), &size.s[1]);


	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	cl_event tempEvent;

	// Queue the kernel up for execution
	errNum = clEnqueueNDRangeKernel(
		clHandles.commandQueue,
		clHandles.kernel.at(kernelID),
		2,
		NULL,
		globalWorkSize,
		localWorkSize,
		0,
		NULL,
		&tempEvent);
	if (errNum != CL_SUCCESS){ std::cerr << "Error queuing kernel for execution." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }

	clWaitForEvents(1, &tempEvent);

}

void BuildGaussianPyramid(ImageBuffer mask, vector<ImageBuffer> &pyramidGaussian, vector<cl_uint2> dimensions)
{
	ImageBuffer *current = &mask;
	pyramidGaussian.push_back(mask);

	for (int i = 0; i < PYRAMID_SIZE; i++)
	{
		ImageBuffer pyrDown = createImageBuffer(dimensions.at(i + 1), sizeof(cl_float4));
		ResizeDown(*current, pyrDown, dimensions.at(i + 1));
		pyramidGaussian.push_back(pyrDown);
		*current = pyrDown;
	}



}

void BlendLayer(vector<ImageBuffer> &blendingImages, ImageBuffer &gaussianPyramid, ImageBuffer  &result, cl_uint2 &size)
{

	ImageBuffer currentResult = createImageBuffer(size, sizeof(cl_float4));
	ImageBuffer multiPlyResult = createImageBuffer(size, sizeof(cl_float4));
	cl_event *tempEvent = 0;

	MatrixMultiplication(blendingImages.at(0), gaussianPyramid, currentResult, size, 0, &currentResult.imageEvent);

	for (int i = 1; i < blendingImages.size(); i++)
	{
		MatrixMultiplication(blendingImages.at(i), gaussianPyramid, multiPlyResult, size, tempEvent, &multiPlyResult.imageEvent);
		AddImages(currentResult, multiPlyResult, currentResult, size, &multiPlyResult.imageEvent, &currentResult.imageEvent);
		tempEvent = &currentResult.imageEvent;
	}

	clWaitForEvents(1, &currentResult.imageEvent);
	result = currentResult;
}

void Blending(const vector<vector<ImageBuffer>> &blendSource,
	vector<ImageBuffer> &gaussianPyramid,
	vector<ImageBuffer> &baseLevels,
	vector<cl_uint2> &pyramidDimensions,
	vector<ImageBuffer> &blendingResults,
	ImageBuffer &resultBaseLevel)
{

	vector<ImageBuffer> blendingImages;

	for (int i = 0; i < baseLevels.size(); i++)
		blendingImages.push_back(baseLevels.at(i));

	BlendLayer(blendingImages, gaussianPyramid.back(), resultBaseLevel, pyramidDimensions.back());









	for (int i = 0; i < blendSource.at(0).size(); i++)
	{
		vector<ImageBuffer> currentBlendingImages;
		ImageBuffer result = createImageBuffer(pyramidDimensions.at(i), sizeof(cl_float4));

		for (int j = 0; j < blendSource.size(); j++)
		{
			currentBlendingImages.push_back(blendSource.at(j).at(i));
		}

		BlendLayer(currentBlendingImages, gaussianPyramid.at(i), result, pyramidDimensions.at(i));


		blendingResults.push_back(result);
	}
}


//Image &reconstructedImage, vector<Image> resultBlending, Image resultBaseLevel, vector<Image> &reconstructionVector)
void LaplacianPyramidReconstruction(vector<ImageBuffer> &blendingRes, ImageBuffer &resultBaseLevel, vector<cl_uint2> &dimensions, vector<ImageBuffer> &gaussian, ImageBuffer &result)
{

	ImageBuffer *resultImage = &resultBaseLevel;

	cl_event *tempEvent = 0;
	ImageBuffer up = createImageBuffer(dimensions.at(0), sizeof(cl_float4));
	ImageBuffer temp = createImageBuffer(dimensions.at(0), sizeof(cl_float4));

	for (int i = blendingRes.size() - 1; i >= 0; i--)
	{

		PyramidUpLocal(resultBaseLevel, up, gaussian.at(i + 1), dimensions.at(i), tempEvent, &up.imageEvent);
		AddImagesBlending(up, blendingRes.at(i), temp, dimensions.at(i), &up.imageEvent, &temp.imageEvent);



		*resultImage = temp;
		tempEvent = &temp.imageEvent;
	}


	clWaitForEvents(1, &temp.imageEvent);
	result = *resultImage;

}

void SaveImages(vector<ImageBuffer> imageVectorBuffer, const char *fileName, vector <cl_uint2> pyramidsDimensions, int offset = 0)
{
	for (int i = 0; i < imageVectorBuffer.size(); i++)
	{
		Image result;
		ReadBuffer4F(imageVectorBuffer.at(i), result, pyramidsDimensions.at(i + offset));

		string path;
		path = fileName + to_string(i);
		path = path + ".png";

		SaveImage(result, path.c_str());
	}

}

unsigned int verifyZeroCopyPtr(void *ptr, unsigned int sizeOfContentsOfPtr)
{
	int status; //so we only have one exit point from function
	if ((unsigned int)ptr % 4096 == 0)
	{
		if (sizeOfContentsOfPtr % 64 == 0)
		{
			status = 1;
		}
		else status = 0;
	}
	else status = 0;
	return status;
}


//HoRuS001
void main()
{
	InitCL("kernels.cl");
	InitKernels();
	InitBuffers();

	vector<cl_uint2> dimensions;
	vector<vector<LaplacianBuffer>> lapBuffers;
	vector<ImageBuffer> laplacian0, laplacian1, laplacian2, laplacian3, laplacian4, laplacian5, gaussian;
	vector<vector<ImageBuffer>> lapPyramids;
	vector<ImageBuffer> baseLevels, imagesBuffers, blendingResults, reconstruction;
	vector<Image> images;
	Image image0, image1, image2, image3, image4, image5, result;
	ImageBuffer base0, base1, base2, base3, base4, base5, resultBaseLevel, mainResult;

	//LoadImage(image0, "D://1K.png", 2);

	//LoadImage(image0, "C://Images//nebula.png", 2);
	LoadImage(image0, "D://4K.png", 89);
	LoadImage(image1, "D://4K2.png", 89);
	
	

	cl_uint2 dimensionsTest = { image0.imageInfo.width, image0.imageInfo.height };


	ImageBuffer srcBuf0 = createImageBuffer(image0.imageBytes, &image0.imageInfo,
CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
		CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
		dimensionsTest,
		sizeof(cl_uchar) * 4);


	ImageBuffer tempMask = createImageBuffer(dimensionsTest, sizeof(cl_uchar) * 4);

	cl_uchar * localOutputArray = new cl_uchar[dimensionsTest.s[0] * dimensionsTest.s[1] * 4];
	cl_event tempEv, readBufEv;

	
	clock_t begin = clock();
	Braka(srcBuf0, tempMask, dimensionsTest, 0, &tempEv);

	size_t localWorkSize[2] = { 16, 16 };
	size_t globalWorkSize[2] = { dimensionsTest.s[0], dimensionsTest.s[1] };
	cl_int errNum;

	errNum = clEnqueueReadBuffer(clHandles.commandQueue,
		tempMask.imageData,
		CL_TRUE,
		0,
		sizeof(cl_uchar)* dimensionsTest.s[0] * dimensionsTest.s[1] * 4,
		localOutputArray,
		0,
		&tempEv,
		&readBufEv);

	clWaitForEvents(1, &readBufEv);

	clock_t end = clock();

	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << elapsed_secs << endl;

	

	if (errNum != CL_SUCCESS) { std::cerr << "Error setting kernel arguments." << std::endl; Cleanup(clHandles.context, clHandles.commandQueue, clHandles.program, clHandles.kernel); }
	


	result.imageInfo = image0.imageInfo;
	result.imageFloat = NULL;
	result.imageFloat4 = NULL;
	result.imageShort = NULL;
	result.imageBytes = localOutputArray;


	SaveImage(result, "D://results//result2.png");





	system("PAUSE");
}


