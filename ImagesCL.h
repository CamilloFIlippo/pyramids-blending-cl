#ifndef IMAGESCL_H_
#define IMAGESCL_H_

#include <CL/cl.h>
#include <vector>

const unsigned int extendMaskWidth = 5;
const unsigned int extendMaskHeight = 5;

enum PixelFormats{ RGB, RGBA, BGR, BGRA };



float extendMask[extendMaskWidth][extendMaskHeight] =
{

	{ 0.0025, 0.0125, 0.0200, 0.0125, 0.0025 },
	{ 0.0125, 0.0625, 0.1000, 0.0625, 0.0125 },
	{ 0.0200, 0.1000, 0.1600, 0.1000, 0.0200 },
	{ 0.0125, 0.0625, 0.1000, 0.0625, 0.0125 },
	{ 0.0025, 0.0125, 0.0200, 0.0125, 0.0025 },
};


static const float extendMask2[extendMaskWidth * extendMaskHeight] =
{

	(1 / 256.0f), (4 / 256.0f),  (6 / 256.0f),  (4 / 256.0f),  (1 / 256.0f),
	(4 / 256.0f), (16 / 256.0f), (24 / 256.0f), (16 / 256.0f), (4 / 256.0f),
	(6 / 256.0f), (24 / 256.0f), (36 / 256.0f), (24 / 256.0f), (6 / 256.0f),
	(4 / 256.0f), (16 / 256.0f), (24 / 256.0f), (16 / 256.0f), (4 / 256.0f),
	(1 / 256.0f), (4 / 256.0f),  (6 / 256.0f),  (4 / 256.0f),  (1 / 256.0f)
};


static const float gaussian5[5 * 5] = {
	1.0 / 273.0, 4.0 / 273.0, 7.0 / 273.0, 4.0 / 273.0, 1.0 / 273.0,
	4.0 / 273.0, 16.0 / 273.0, 26.0 / 273.0, 16.0 / 273.0, 4.0 / 273.0,
	7.0 / 273.0, 26.0 / 273.0, 41.0 / 273.0, 26.0 / 273.0, 7.0 / 273.0,
	4.0 / 273.0, 16.0 / 273.0, 26.0 / 273.0, 16.0 / 273.0, 4.0 / 273.0,
	1.0 / 273.0, 4.0 / 273.0, 7.0 / 273.0, 4.0 / 273.0, 1.0 / 273.0,
};

cl_short extendMaskInvert[extendMaskWidth][extendMaskHeight] =
{
	{ 400.0000, 80.0000, 50.0000, 80.0000, 400.0000 },
	{ 80.0000, 16.0000, 10.0000, 16.0000, 80.0000 },
	{ 50.0000, 10.0000, 6.0000, 10.0000, 50.0000 },
	{ 80.0000, 16.0000, 10.0000, 16.0000, 80.0000 },
	{ 400.0000, 80.0000, 50.0000, 80.0000, 400.0000 },
};

struct ImageInfo
{
	cl_uint width;
	cl_uint height;
	cl_uint channels;
	PixelFormats pixelFormat;
};

struct Image
{
	ImageInfo imageInfo;
	cl_uchar* imageBytes;
	cl_short* imageShort;
	cl_float* imageFloat;
	cl_float4* imageFloat4;

	Image()
	{
		this->imageBytes = NULL;
		this->imageFloat = NULL;
		this->imageShort = NULL;
	};

	Image(ImageInfo imageInfo, cl_uchar* imageBytes)
	{
		this->imageInfo = imageInfo;
		this->imageBytes = imageBytes;
	}

	Image(ImageInfo imageInfo, cl_short* imageShort)
	{
		this->imageInfo = imageInfo;
		this->imageShort = imageShort;
	}

	Image(ImageInfo imageInfo, cl_float* imageFloat)
	{
		this->imageInfo = imageInfo;
		this->imageFloat = imageFloat;
	}
	
};

struct ImageBuffer
{
	cl_mem imageData;
	cl_mem imageInfo;
	cl_event imageEvent;

	ImageBuffer(cl_mem imageData, cl_mem imageInfo)
	{
		this->imageData = imageData;
		this->imageInfo = imageInfo;
	}

	ImageBuffer()
	{
		this->imageData = NULL;
		this->imageInfo = NULL;
	}

};

struct LaplacianBuffer
{
	ImageBuffer pyramidDown;
	ImageBuffer pyramidUp;
	ImageBuffer subtractResult;
};



struct clHandle
{
	cl_context              context;
	cl_device_id            *devices;
	cl_command_queue        commandQueue;
	cl_program              program;
	std::vector<cl_kernel>  kernel;
	char * compilerOptions;

}clHandles;


#endif    //#ifndef IMAGESCL_H_